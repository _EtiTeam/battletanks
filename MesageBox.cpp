#include "Engine.hpp"
#include "MessageBox.hpp"
#include <windows.h>


enum TypeButton
{
	PLAY = 0,
	NETWORK = 1,
	EXIT = 2
};
Menu::Menu(Engine* engine)

{
	this->setActive(true);
	this->setVisible(true);
	currentButton = 0;
	buttons.push_back(new Button(engine, PLAY));
	buttons.push_back(new Button(engine, NETWORK));
	buttons.push_back(new Button(engine, EXIT));
	this->engine = engine;
	this->position = Vector2u(engine->SCREEN_WIDTH / 4, engine->SCREEN_HEIGHT / 5);
	this->size = Vector2f(400.0, 500.0);
	menuBackgroundPath = "images/toolbar.png";
	if (!menuBackgroundTexture.loadFromFile(menuBackgroundPath))
		printf("error obrazka");

	menuBackground.setTexture(&menuBackgroundTexture);
	menuBackground.setSize(size);
	menuBackground.setPosition(static_cast<float>(position.x) + 50, static_cast<float>(position.y));

	if (!this->menuFont.loadFromFile("fonts/comic.ttf")) throw "Brak czcionki";

	menuLabel.setString("Menu");
	menuLabel.setColor(Color::White);
	menuLabel.setPosition(static_cast<float>(position.x) + size.x / 2 + 10, 0.01*size.y + position.y + 30);
	menuLabel.setCharacterSize(32);
	menuLabel.setFont(menuFont);
	//playButton2.setActive(true);



}

void Menu::draw()
{
	if (isActive())
	{

		auto window = engine->getGameWindow();
		if (isVisible()){
			window->draw(menuBackground);
			window->draw(menuLabel);


			for (int it = 0; it < buttons.size(); ++it)
			{
				if (it == currentButton)
					buttons.at(it)->setActive(true);
				else
				{
					buttons.at(it)->setActive(false);
				}
				buttons.at(it)->draw();
			}
		}
	}

}

bool Menu::isActive()
{
	return active;
}

void Menu::setActive(bool active)
{
	this->active = active;
}
bool Menu::isVisible()
{
	return visible;
}

void Menu::setVisible(bool visible)
{
	this->visible = visible;
}

void Menu::changeActive()
{
	this->active = (!active);
}
void Menu::previousButton()
{
	if (currentButton>0)
		this->currentButton--;
	Sleep(200);
}
void Menu::nextButton()
{
	if (currentButton<buttons.size() - 1)
		this->currentButton++;
	Sleep(200);
}

void Menu::escape()
{
	buttons[currentButton]->escape();
}

void Menu::enter()
{
	buttons[currentButton]->enter();
}
