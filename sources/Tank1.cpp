#include "Tank1.hpp"
#include "EngineClient.hpp"

Tank1::Tank1(size_t id, EngineBase* engine, Point& startLRPoint, GameEvents* gameEvents, Color tankColor)
	: 
	Tank(id, engine, startLRPoint, gameEvents),
//	textureSpace(20, 16, 235, 147),
//	texturePath("images/panzer.png"),
	tankColor(tankColor),
	destroyed(false)
{
	tankShape.hideOutline();
	tankShape.hideRectColor();
	
	isGraphicInited = false;
}

Tank1::Tank1(EngineBase* engine, Json::Value& objectSrc)
	:
	Tank(engine, objectSrc),
	tankColor(objectSrc[TANK_COLOR].asUInt()),
	destroyed(objectSrc[TANK_DESTROYED].asBool())
{
	isGraphicInited = false;
	//tankShape.setTexture(texturePath, textureSpace);
	//tankShape.setFillColor(tankColor);
}

void Tank1::serialize(Json::Value& objectDest)
{
	//static Json::Value textureSpaceJson;
	//textureSpace.serialize(textureSpaceJson);

	Tank::serialize(objectDest);
	objectDest[TANK_COLOR] = tankColor.toInteger();
	objectDest[TANK_DESTROYED] = destroyed;
}

void Tank1::merge(GameObject* gameObject)
{
	Tank::merge(gameObject);

	auto object = reinterpret_cast<Tank1*>(gameObject);
	if (object->isDestroyed())
		destroy();
}

void Tank1::initGraphic()
{
	if (isGraphicInited == false){
		isGraphicInited = true;
		tankShape.setTexture("images/panzer.png", Space(20, 16, 235, 147)); // drugi parametr to 'wnetrze' czolgu
		tankShape.setFillColor(tankColor);
	}
}

void Tank1::destroy()
{	
	if (isDestroyed() == false){
		tankShape.setTexture("images/panzerDestroyed.png", Space(41, 69, 253, 197));
		tankShape.hideRectColor();
		this->destroyed = true;
	}
}
bool Tank1::isDestroyed()
{
	return destroyed;
}

void Tank1::draw()
{
	initGraphic();
	tankShape.draw();
}

CanMove Tank1::collision(GameObject* withObject)
{
	
	if (withObject->getObjectType() == BULLET)
	{
		if (lives > 0)
		{
			lives--;
			return YES_CAN;
		}
		else
		{
			destroy();
			return NO_CANT;
		}
	}	 
	return NO_CANT;
}

int Tank1::getLives()
{
	return lives;
}

CanMove Tank1::isMovable()
{
	if (this->isDestroyed())
		return NO_CANT;
	else
		return YES_CAN;
}

ObjectType Tank1::getObjectType()
{
	return TANK1;
}
