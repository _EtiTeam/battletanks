#include "GameObjectFactory.hpp"
#include "Tank1.hpp"
#include "Bullet.hpp"

GameObjectFactory::GameObjectFactory(EngineBase* engine)
	:engine(engine)
{
}

GameObject* GameObjectFactory::get(ObjectType objectType, Json::Value& jsonSrc)
{
	switch (objectType)
	{
	case TANK1: return new Tank1(engine, jsonSrc);
	case BULLET: return new Bullet(engine, jsonSrc);
	}
	throw "object type not found";
}
