#pragma once
#ifndef _LEVEL_HPP_
#define _LEVEL_HPP_

#include <list>
#include <SFML/Graphics/Color.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Texture.hpp>

#include "Space.hpp"
#include "GameObjectSet.hpp"

using std::list;
using sf::Color;
using sf::Sprite;
using sf::Texture;
using std::string;

class EngineBase;
class GameObject;
class StaticObject;

class Level abstract
{
public:
	Level(EngineBase* engine); // budowa statycznych obiektow w staticObjects	
	virtual ~Level();
	virtual Space& getLevelSpace() = 0;

	bool isAvailable(Space& space); // sprawdzenie tylko dla elementow statycznych tworzonych przez level
	
	Color getBackgroundColor();
	Sprite& getBackroundSprite();
	GameObjectSet* getLevelStaticObjects();
	
	
protected:
	void setBackground(string backgroundPath);	
	virtual void drawLineHorizontal(float x1, float x2, float y) final;
	virtual void drawLineVertical(float y1, float y2, float x) final;
	EngineBase* engine;
	GameObjectSet staticObjects;	// elementy dynamiczne dodaje sie do objectMenager
	Color backgroundColor;
	Sprite backgroundSprite;
	Texture backgroundTexture;
	size_t idNumeration;
};

#endif