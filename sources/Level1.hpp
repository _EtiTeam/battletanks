#pragma once
#ifndef _LEVEL1_HPP_
#define _LEVEL1_HPP_

#include "Level.hpp"

class Level1
	: public Level
{
public:
	Level1(EngineBase* engineBase); // budowa statycznych obiektow w ObjectManager
	~Level1();
	
	Space& getLevelSpace() override;
	
private:
	Space levelSpace;
};

#endif