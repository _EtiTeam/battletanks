#pragma once
#ifndef _EMPTY_OBJECT_HPP_
#define _EMPTY_OBJECT_HPP_

#include "GameObject.hpp"

// obiekt sluzacy do porownywania obiektow
// jedyny obiekt ktoremu mozna zmienic id
class EmptyObject final
	: public GameObject
{
public:
	EmptyObject(size_t id);
	virtual ~EmptyObject() {}

	void draw() override				{}
	void update() override				{}
	void update(Time& time) override	{}

	int getZIndex() override			{ return 0; }
	int getLives(void) override			{ return 0; }
	void unbindGameEvents() override	{}
	ObjectType getObjectType() override { return EMPTY_OBJECT; };

	void setId(size_t id);
	
};

#endif