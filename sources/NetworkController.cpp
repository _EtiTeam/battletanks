#define WIN32_LEAN_AND_MEAN
#include <windows.h>

#include "NetworkController.hpp"
#include "otherFunctions.hpp"
#include "EngineClient.hpp"

#include "ServerController.hpp"
#include "json/json.h"
#include <iostream>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>

#pragma comment (lib, "Ws2_32.lib")

/////////////////// WSA_INITIALIZER ////////////////////////////////////
NetworkController::WSAInitalizer NetworkController::WSAInitalizer_; // wywolanie konstruktora

NetworkController::WSAInitalizer::WSAInitalizer()
{
	static WSADATA wsadata;
	auto result = WSAStartup(MAKEWORD(2, 2), &wsadata);
	if (result != NO_ERROR)
		throw ConnectionException("nie udalo sie utworzyc WSAdata");
}

NetworkController::WSAInitalizer::~WSAInitalizer()
{
	WSACleanup();
}

/////////////////// EXCEPTIONS ////////////////////////////////////

NetworkController::ConnectionException::ConnectionException(const char* reason): exception(reason)
{
	cerr << reason << ": " << WSAGetLastError() << endl;
}

/////////////////// NETWORK_CONTROLLER /////////////////////////////
NetworkController::NetworkController(EngineBase* engine)
	: 
	engine(engine)
{
	InitializeCriticalSection(&criticalSection);
}

NetworkController::~NetworkController()
{
	for (auto it = threadHandles.begin(); it != threadHandles.end(); ++it)
		TerminateThread(*it, 0);

	DeleteCriticalSection(&criticalSection);
}

void NetworkController::addThreadHandle(HANDLE threadId)
{
	EnterCriticalSection(&criticalSection);
	threadHandles.push_back(threadId);
	LeaveCriticalSection(&criticalSection);
}

DWORD NetworkController::startServer(void* connectCfgVoid)
{
	auto connectCfg = static_cast<ConnectCfg*>(connectCfgVoid);
	int iResult;

	SOCKET serverSocket = INVALID_SOCKET;
	SOCKET acceptSocket = INVALID_SOCKET;

	struct addrinfo *result = nullptr;
	struct addrinfo hints;

	// ########### inicjalizacja ##################
	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;

	iResult = getaddrinfo(nullptr, toString(connectCfg->port).c_str(), &hints, &result);
	if (iResult != 0) {
		throw ConnectionException("getaddrinfo failed with error \n");
	}

	serverSocket = ::socket(result->ai_family, result->ai_socktype, result->ai_protocol);
	if (serverSocket == INVALID_SOCKET) {
		throw ConnectionException("Socket failed \n");
	}
	iResult = ::bind(serverSocket, result->ai_addr, static_cast<int>(result->ai_addrlen));
	if (iResult == SOCKET_ERROR) {
		throw ConnectionException("Bind failed with error\n");
	}
	freeaddrinfo(result);
	iResult = listen(serverSocket, SOMAXCONN);
	if (iResult == SOCKET_ERROR) {
		throw ConnectionException("Listen failed with error \n");

	}
	printf("\nSerwer pomyslnie wystartowal!\nOczekiwanie na klienta...\n");

	//////////////// AKCEPTACJA PRZYCHODZACYCH POLACZEN ////////////////////
	DWORD threadId;
	list<ServiceCfg> list;
	
	cout << "uruchomiono serwer port " << connectCfg->port << endl;
	while (connectCfg->engine->isOpen())
	{
		acceptSocket = accept(serverSocket, nullptr, nullptr);
		if (acceptSocket == INVALID_SOCKET) 
			throw ("akceptacja socketu z bledem", WSAGetLastError());

	
		char type;
		::recv(acceptSocket, &type, sizeof(char), 0); // odczytanie typu klienta

		if (type == RECEIVE)		// dla 0
			connectCfg->servicingFunction = ServerController::serviceSendingSocket;
		else if (type == SENDING)	// dla 1
			connectCfg->servicingFunction = ServerController::serviceReceivingSocket;
		else
			throw ConnectionException("nie otrzymano poprawnego typu clienta");

		// DEFINIOWANIE STRUKTURY DLA NOWEGO WATKU
		list.push_back({ connectCfg->engine, acceptSocket, connectCfg->networkController });
		auto threadStruct = static_cast<void*>(&list.back());
		// ROZPOCZENIE DZIALAN NOWEGO WATKU
		auto handle = CreateThread(nullptr, 1 << 24, connectCfg->servicingFunction, threadStruct, 0, &threadId);
		connectCfg->networkController->addThreadHandle(handle);
		//SetThreadPriority(&thredId, THREAD_MODE_BACKGROUND_BEGIN);
		
	}
	// TODO wylaczyc przechwytywanie jesli jest graczy == maxILOSC	
	closesocket(serverSocket); // TODO czy po zamknieciu mozna obslugiwac
	return 0;
}

DWORD NetworkController::startClient(void* connectCfgVoid)
{
	auto connectCfg = static_cast<ConnectCfg*>(connectCfgVoid);

	// ########### INICJALIZACJA ##################
	SOCKET clientSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (clientSocket == INVALID_SOCKET)
		throw ConnectionException("nie udalo sie utworzyc socketa: ");

	int iResult;
	struct addrinfo *result = nullptr, *ptr = nullptr, hints;

	//sockaddr_in sockaddrStruct;
	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	iResult = getaddrinfo(connectCfg->addr.c_str(), toString(connectCfg->port).c_str(), &hints, &result);
	if (iResult != 0)
		throw ConnectionException("getaddrinfo utworzono z niepowodzeniem");

	cout << "proba laczenia klienta pod adres " << connectCfg->addr << " port " << connectCfg->port << endl;

	for (ptr = result; ptr != nullptr; ptr = ptr->ai_next) {
		clientSocket = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);
		if (clientSocket == INVALID_SOCKET)
			throw ConnectionException("socket blad");

		iResult = connect(clientSocket, ptr->ai_addr, static_cast<int>(ptr->ai_addrlen));
		if (iResult == SOCKET_ERROR) {
			closesocket(clientSocket);
			clientSocket = INVALID_SOCKET;
			continue;
		}
		break;
	}
	freeaddrinfo(result);

	try{
		if (clientSocket == INVALID_SOCKET)
			throw ConnectionException("nie polaczono, serwer zajety lub nie dostepny");

		// POINFORMOWANIE SERWERA O TYPIE DANEGO KLIENTA
		char message = static_cast<char>(connectCfg->type);
		::send(clientSocket, &message, sizeof(char), 0);

		// ####### ROZPOCZECIE DZIALAN KLIENTA ###########
		ServiceCfg serviceCfg = { connectCfg->engine, clientSocket, connectCfg->networkController };
		connectCfg->servicingFunction(static_cast<void*>(&serviceCfg));
	} catch (ConnectionException e)
	{
		closesocket(clientSocket);
		auto engine = dynamic_cast<EngineClient*>(connectCfg->engine);
		engine->networkDisconnect();
		return 1;
	}

	closesocket(clientSocket); 
	return 0;
}

void NetworkController::send(SOCKET& socket, Json::Value& jsonToSend)
{
	Json::FastWriter fw;
	fw.omitEndingLineFeed();
	auto strToSend = fw.write(jsonToSend);
	int sendedBytes;

	auto length = toString(strToSend.size());
	//auto y = length.c_str();				// TODO zmiene do usuniecie
	//auto x = strlen(length.c_str());		// pomocne w debbugowaniu
	
	// wysylanie ilosc znakow wraz z null - odroznienie liczby od danych
	sendedBytes = ::send(socket, length.c_str(), length.length() + 1, 0);
	if (sendedBytes <= 0 || sendedBytes == WSAECONNRESET)
		throw ConnectionException("WYSYL: polaczenie z zostalo zerwane");
	else if (sendedBytes != length.length() + 1)
		throw ConnectionException("WYSYL: przeslano niepelna wartosc");

	// wysylanie danych
	sendedBytes = ::send(socket, strToSend.c_str(), strToSend.length(), 0);
	if (sendedBytes <= 0 || sendedBytes == WSAECONNRESET)
		throw ConnectionException("WYSYL: polaczenie z zostalo zerwane");
	else if (sendedBytes != strToSend.length())
		throw ConnectionException("WYSYL: przeslano niepelna paczke danych");

	// odebranie potwierdzenie odbioru - blokowanie do momentu odbioru
	char c;
	::recv(socket, &c, sizeof(c), 0);
}

void NetworkController::recv(SOCKET& socket, Json::Value& jsonDest, char * buffChar, string& strBuff)
{
	// wartosc max nubmer nie moze byc za duza zeby nie zmiescila sie w niej 2 send'y
	static const unsigned MAX_NUMBER_DIGIT = 11;
	strBuff.clear();

	int bytes = ::recv(socket, buffChar, MAX_NUMBER_DIGIT, 0);
	if (bytes <= 0)
		throw ConnectionException("ODBIOR: polaczenie z zostalo zerwane");

	unsigned allBytes = stoul(buffChar);
	int digitsChars = numberOfDigits(allBytes) + 1;		// +1 znak /0 po liczbie

	// odebranie danych zawartych po liczbach
	unsigned receivedBytes = bytes - digitsChars;
	strBuff.append(buffChar + digitsChars, receivedBytes);

	// odebranie dalszych danych
	while (receivedBytes < allBytes){
		auto neededBytes = allBytes - receivedBytes;
		bytes = ::recv(socket, buffChar, (neededBytes > BUFF_SIZE) ? BUFF_SIZE : neededBytes, 0);
		if (bytes < 0)
			throw ConnectionException("ODBIOR: polaczenie z zostalo zerwane");
		strBuff.append(buffChar, bytes);
		receivedBytes += bytes;
	}

	// wylanie potwierdzenie odbioru
	char c = 'Y';
	::send(socket, &c, sizeof(c), 0);

	//cout << "odebrano " << strBuff;
	Json::Reader reader;
	jsonDest.clear();
	reader.parse(strBuff, jsonDest);
}
