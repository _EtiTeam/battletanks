#include "Button.hpp"
#include "EngineClient.hpp"
#include <SFML/Graphics.hpp>
enum TypeButton
{
	PLAY = 0,
	NETWORK = 1,
	EXIT = 2
};
Button::Button(EngineClient* engine, int type)
{
	this->engine = engine;
	
	Vector2f* size = new Vector2f(400.0, 500.0);
	this->position = Vector2u(engine->SCREEN_WIDTH / 4, engine->SCREEN_HEIGHT / 5);
	if (type == PLAY)
	{
		this->type = PLAY;
		playBtnPath = "images/btn_play.png";
		playBtnPath_active = "images/btn_play_active.png";
		playBtn.setPosition(static_cast<float>(position.x) + 400/ 2-size->x/8, 0.01*size->y + 120 + 100);
	}
	if (type == NETWORK)
	{
		this->type = NETWORK;
		playBtnPath = "images/btn_network.png";
		playBtnPath_active = "images/btn_network_active.png";
		playBtn.setPosition(static_cast<float>(position.x) + 400 / 2 - size->x / 8, 0.01*size->y + 220 + 100);
	}
	if (type == EXIT)
	{
		this->type = EXIT;
		playBtnPath = "images/btn_exit.png";
		playBtnPath_active = "images/btn_exit_active.png";
		playBtn.setPosition(static_cast<float>(position.x) + 400 / 2 - size->x / 8, 0.01*size->y + 320 + 100);
	}


	if (!playBtnTexture.loadFromFile(playBtnPath))
		throw "error obrazka";
	if (!playBtnTexture_active.loadFromFile(playBtnPath_active))
		throw "error obrazka";
	
	playBtn.setTexture(&playBtnTexture);
	playBtn.setSize(Vector2f(200.0, 50.0));
	
}
void Button::draw()
{
	auto window = engine->getGameWindow();
	window->draw(playBtn);
}

void Button::setActive(bool value)
{
	this->active = value;
	if (active)
	{
		
		playBtn.setTexture(&playBtnTexture_active);
	}
	else
	{
	
		playBtn.setTexture(&playBtnTexture);
	}

}

bool Button::isActive()
{
	return this->active;
}

void Button::enter()
{
	if (type == PLAY)
	{
		engine->getMenu()->setActive(false);	
		engine->setGameRunning(true);
		//*DONE umie�ci� mo�e komunikat do serwera o rozpocz�ciu gry 
	}
	if (type == NETWORK)
	{
		engine->getMenu()->subMenu();
	}
	if (type == EXIT)
	{
		engine->getGameWindow()->close();
	}
}

void Button::escape()
{
	if (type == NETWORK)
	{
		
		engine->getMenu()->mainMenu();
	}
}
