#pragma once
#ifndef _MOTION_HPP_
#define _MOTION_HPP_
#include <SFML/System/Vector2.hpp>

#define DEFAULT_TEMPO 400.0f
#define DEFAULT_SPEED_DOWN_IDLE	DEFAULT_TEMPO / 5.0f
#define DEFAULT_SPEED_DOWN		1.5f * DEFAULT_TEMPO
#define DEFAULT_SPEED_UP		DEFAULT_TEMPO
#define DEFAULT_MAX_SPEED		3.0f * DEFAULT_TEMPO

namespace sf {
	class Time;
}

using sf::Vector2f;
using sf::Time;

namespace Json
{
	class Value;
}

class EngineClient;
class DynamicObject;
enum Direction;

struct SpeedProperty
{
	float speedDownIdle;	// hamowanie, spowodowane oporami toczenia
	float speedDown;		// hamowanie, spowodowane zmiana kierunku
	float speedUp;			// przyspieszenie
	float maxSpeed;		
};

class Motion
{
public:
	Motion(DynamicObject& dynamicObject, const SpeedProperty& speedProperty 
		= { DEFAULT_SPEED_DOWN_IDLE, DEFAULT_SPEED_DOWN, DEFAULT_SPEED_UP, DEFAULT_MAX_SPEED });
	Motion(DynamicObject& dynamicObject, Json::Value& objectSrc);

	virtual void serialize(Json::Value& objectDest);
	
	void update();					// oparte na czasie w silniku
	void update(float deltaTime);	// opadte tylko na podanym czasie
	float getXSpeed();
	float getYSpeed();
	// wraz z sprawdzeniem czy isMovable (metoda slu�y tylko do obs�ugi sterowania przez u�ytkownika)
	// isTimeBased == false tylko dla predkosci zmieniennych jednokrotnie (brak ingerencji uzytkownika w predkosc)
	void speedUp(Direction direction, bool isTimeBased = true);
	void merge(const Motion& motion);

	// przmieszczenie zgodna z podanym parametrem
	// zatrzymuje obiekt i wywoluje kolizje
	bool moveX(float xMovement);
	bool moveY(float yMovement);
	void setSpeedProperty(const SpeedProperty& spdp);
protected:
	void leftSpeedUp(bool isTimeBased);
	void rightSpeedUp(bool isTimeBased);
	void upSpeedUp(bool isTimeBased);
	void downSpeedUp(bool isTimeBased);
	void idleXSpeedDown();
	void idleYSpeedDown();

	// funkcje przemieszczajace po 1 pikselu
	// w razie kolizji calkowicie zatrzymuja ruch
	bool moveX();
	bool moveY();

	float getDeltaTimeSeconds();
	
private:
	DynamicObject& dynamicObject;
	SpeedProperty speedProperty;
	float xSpeed;
	float ySpeed;
	float xMovement;
	float yMovement;

};

#endif