#pragma once
#ifndef _WALL_HPP_
#define _WALL_HPP_

#include "StaticObject.hpp"
#include "GameRect.hpp"
#include "Point.hpp"
// postac gracza
class Wall
	: public StaticObject
{
public:
	Wall(size_t id, EngineBase* engine, Point& p);
	Wall(EngineBase* engine, Json::Value& objectSrc);

	void update() override;
	void draw() override;
	//CanMove collision(GameObject* withObject) override;
	int getZIndex() override;
	virtual ObjectType getObjectType() override final;
	CanMove collision(GameObject* withObject) override final;
	int getLives() override final;
private:
	GameRect wallShape;
	int lives;
	const static int Wall_WIDTH = 20;
	const static int Wall_HEIGHT = 20;
};


#endif