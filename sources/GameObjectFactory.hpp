#pragma once
#ifndef _GAME_OBJECT_FACTORY_HPP_
#define _GAME_OBJECT_FACTORY_HPP_

#include "GameObject.hpp"

struct GameObjectFactory
{
public:
	GameObjectFactory(EngineBase* engine);
	GameObject* get(ObjectType objectType, Json::Value& jsonSrc);
private:
	EngineBase* engine;

};



#endif