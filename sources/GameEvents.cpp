#include "GameEvents.hpp"
#include "EngineClient.hpp"

using namespace sf;

GameEvents::GameEvents(EngineBase* engine, bool readAllKey)
{
	this->engine = engine;
	this->readAllKey = readAllKey;
	closeSignal = false;
	control = NONE_CTRL;
	blockingDirection = NONE;
}

GameEvents::GameEvents(Json::Value& json)
{
	engine = nullptr;
	readAllKey = false;
	closeSignal = json["closeSignal"].asBool();
	control = static_cast<Control>(json["control"].asInt());
	blockingDirection = static_cast<Direction>(json["blockingDirection"].asInt());
}

void GameEvents::update()
{
	if (Keyboard::isKeyPressed(Keyboard::Return)) control = ENTER; /*return enter key*/
	else if (Keyboard::isKeyPressed(Keyboard::Escape)) control = ESCAPE;

	if (isReadAllKey()){

		RenderWindow* gameWindow = engine->getGameWindow();
		Event event;

		// zerowanie zmiennych
		blockingDirection = NONE;
		control = NONE_CTRL;

		while (gameWindow->pollEvent(event))
		{
			switch (event.type)
			{
				case Event::Closed:
					gameWindow->close();
					closeSignal = true;
					break;
				case (Event::KeyPressed) :
				{
					if (event.key.code == Keyboard::Escape)
					{
						/*
						gameWindow->close();
						closeSignal = true;*/
						control = ESCAPE;
					}
					else if (event.key.code == Keyboard::Return) control = ENTER;
					else if (event.key.code == Keyboard::Up) blockingDirection = UP;
					else if (event.key.code == Keyboard::Down) blockingDirection = DOWN;
					else if (event.key.code == Keyboard::Left) blockingDirection = LEFT;
					else if (event.key.code == Keyboard::Right) blockingDirection = RIGHT;

					break;
				}
			}
		}
	}
}

void GameEvents::update(Json::Value& json)
{
	closeSignal = json[CLOSE_SIGNAL].asBool();
}

void GameEvents::serialize(Json::Value& json)
{
	json[CLOSE_SIGNAL] = closeSignal;
}


bool GameEvents::isReadAllKey()
{
	return readAllKey;
}

void GameEvents::offReadAllKey()
{
	readAllKey = false;
}

void GameEvents::onReadAllKey()
{
	readAllKey = true;
}

Control GameEvents::getControl()
{
	return control;
}

Direction GameEvents::getBlockedDirection()
{
	return blockingDirection;
}


