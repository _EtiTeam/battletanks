#pragma once
#ifndef _STATIC_OBJECT_HPP_
#define _STATIC_OBJECT_HPP_

#include "GameObject.hpp"

class StaticObject abstract 
	: public GameObject
{
public:
	StaticObject(size_t id, EngineBase* engine, Space& space);
	StaticObject(EngineBase* engine, Json::Value& objectSrc);
	virtual ~StaticObject();

	void update() override;
	void update(Time& deltaTime) override final		{}
	virtual ObjectType getObjectType()=0;
	virtual int getLives() = 0;
	CanMove collision(GameObject* withObject) override ;

	void unbindGameEvents() override {}
private:
};

#endif