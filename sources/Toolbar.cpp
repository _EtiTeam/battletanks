#include "Toolbar.hpp"
#include "EngineClient.hpp"
#include "otherFunctions.hpp"
#include "Tank.hpp"

Toolbar::Toolbar(EngineClient* engine){
	this->engine = engine;
	this->position = Vector2u(10,10);	// offset from left-top corner
	float x = static_cast<float>(engine->getGameWindow()->getSize().x);
	float y = (float)engine->getGameWindow()->getSize().y;
	this->size = Vector2f(x - 20.0f, y / 8.0f); //	size: 780 x 60
	this->it = 0;
	if (!this->font.loadFromFile("fonts/comic.ttf"))
	{
		throw "Brak czcionki";
	}
	
	barPath = "images/toolbar.png";	
	if (!barBackgroundTexture.loadFromFile(barPath))
		printf("error obrazka");
	
	barBackground.setTexture(&barBackgroundTexture);
	barBackground.setSize(size);
	barBackground.setPosition(static_cast<float>(position.x), static_cast<float>(position.y));	
	
	lifeLabel.setString("Remaining lives: ");
	lifeLabel.setColor(Color::White);
	lifeLabel.setPosition(0.75f*size.x + position.x, 0.05f*size.y + position.y);
	lifeLabel.setCharacterSize(23);
	lifeLabel.setFont(font); 
	heartPath = "images/serce.png";
	if (!heartTexture.loadFromFile(heartPath))
		printf("error obrazka");
	for (int i = 0; i < 3; i++)
	{
		heartIcons[i].setTexture(&heartTexture);
		heartIcons[i].setSize(Vector2f(30.0f, 30.0f));
	}
	heartIcons[0].setPosition(0.75f*size.x + position.x, 0.05f*size.y + position.y + 30.0f);
	heartIcons[1].setPosition(0.75f*size.x + position.x + 50.0f, 0.05f*size.y + position.y + 30.0f);
	heartIcons[2].setPosition(0.75f*size.x + position.x + 100.0f, 0.05f*size.y + position.y + 30.0f);
	
	gameIdInfo.setString("Player nr: " + toString(engine->getObjectManager()->getId()));
	gameIdInfo.setColor(Color::White);
	gameIdInfo.setPosition(position.x+20.0f, 0.05f*size.y + position.y);
	gameIdInfo.setCharacterSize(32);
	gameIdInfo.setFont(font);
	connectionStatus.setFillColor(Color::Yellow);
	connectionStatus.setSize(Vector2f(10.0f, 50.0f));
	connectionStatus.setPosition(size.x - 20.0f, 20.0f);

	progressBarSizeX = 0.0f;
	progressBarSizeY = 50.0f;
	progressBarPosX = 0.5f*size.x ;
	tempProgressBarPosX = progressBarPosX;
	progresBar.setFillColor(Color(
		255, 0, 0));
	progresBar.setSize(Vector2f(0.0f, 50.0f));
	progresBar.setPosition(progressBarPosX, 20.0f);
	
}

void Toolbar::draw(){
	if (engine->isConnected())
	{
		connectionStatus.setFillColor(Color::Green);
	}
	else
		connectionStatus.setFillColor(Color::Yellow);
	auto window = engine->getGameWindow();
	if ((clock() - t1)  > 100)
	{
		heartIcons[it++].setSize(Vector2f(30.0, 30.0));
		if (it > 2)it = 0;
		heartIcons[it].setSize(Vector2f(32.0, 32.0));
		t1 = clock();
	}
	Tank* tmp = dynamic_cast<Tank*>(engine->getMainGameUserEvents()->getBindedObject());
 	if (tmp != nullptr && !tmp->isReloaded())
	{
		
		
			progressBarSizeX += 6.0f;
			tempProgressBarPosX -= 3.0f;
			progresBar.setSize(Vector2f(progressBarSizeX, 50.0f));
			progresBar.setPosition(Vector2f(tempProgressBarPosX, 20.0f));		
			window->draw(progresBar);
			

		
	}
	else
	{
		progressBarSizeX = 0.0f;
		tempProgressBarPosX = progressBarPosX;
	}
	window->draw(barBackground);
	auto bindedObject = engine->getMainGameUserEvents()->getBindedObject();	
	if (bindedObject != nullptr)
	{
		int lives = bindedObject->getLives();
		for (int i = 0; i<lives; i++)
		{
			if (lives > i)
				window->draw(heartIcons[i]);
		}		
	}
	window->draw(lifeLabel);
	window->draw(connectionStatus);
	window->draw(gameIdInfo);
}