#include "GameRect.hpp"
#include "Point.hpp"
#define FILL_COLOR Color::Red
#define OUTLINE_COLOR Color::Yellow

using sf::Vector2f;
using sf::IntRect;

//GameRect::GameRect(EngineClient* engine, int xMiddle, int yMiddle, int width, int height, int thick)
//	:
//	RectangleShape(Vector2f(static_cast<float>(width), static_cast<float>(height))),
//	space(xMiddle, yMiddle, width, height, true)
//{
//	// ustanienie srodka obrotu w srodku figury
//	float xMidRelative = space.getWidth() / 2.0;
//	float yMidRelative = space.getHeight() / 2.0;
//	setOrigin(xMidRelative,yMidRelative);
//	RectangleShape::setPosition(space.getXMid(), space.getYMid()); // srodki zgodnie z ustawionym Origin
//
//	shapeSprite = nullptr;
//	this->engine = engine;
//
//	fillColor = FILL_COLOR;
//	outlineColor = OUTLINE_COLOR;
//	this->showColor();
//	this->setOutlineThickness(static_cast<float>(thick));
//}

GameRect::GameRect(EngineBase* engine, Space& space2, float thick)
	:
	RectangleShape(Vector2f(space2.getWidth(), space2.getHeight()))
{
	space = &space2;

	// ustanienie srodka obrotu w srodku figury
	float xMidRelative = space->getWidth() / 2.0f;
	float yMidRelative = space->getHeight() / 2.0f;
	setOrigin(xMidRelative, yMidRelative);
	RectangleShape::setPosition(space->getXMid(), space->getYMid()); // srodki zgodnie z ustawionym Origin

	shapeSprite = nullptr;
	this->engine = engine;

	fillColor = FILL_COLOR;
	outlineColor = OUTLINE_COLOR;
	this->showColor();
	this->setOutlineThickness(thick);
}

GameRect::~GameRect()
{
	delete shapeSprite;
}

float GameRect::getXMid()
{
	return space->getXMid();
}

void GameRect::move(float x, float y)
{
	space->move(x, y);
}

void GameRect::setPosition(Space& newSpace)
{
	space->setPosition(newSpace);
}

float GameRect::getRotation()
{
	return space->getRotation();
}

void GameRect::setRotation(float angle)
{
	// warunek niedopuszczalny - nie ma pewnosci ze getRotation() jest a kierunkiem obrazka
	//if (RectangleShape::getRotation() != angle) 
	//{
		//RectangleShape::setRotation(angle);
		space->setRotation(angle);
		RectangleShape::setSize(Vector2f(space->getWidth(), space->getHeight()));

		// mogla nastapic zmiana rozmiarow - ponowne ustawienie srodka
		float xMidRelative = space->getWidth() / 2.0f;
		float yMidRelative = space->getHeight() / 2.0f;
		RectangleShape::setOrigin(xMidRelative, yMidRelative);

		if (shapeSprite != nullptr){
			shapeSprite->setRotation(angle);
		}
	//}
}

void GameRect::setFillColor(Color color)
{
	fillColor = color;
	showColor();
}

void GameRect::setOutlineColor(Color color)
{
	outlineColor = color;
	showOutline();
}

Color GameRect::getFillColor()
{
	return fillColor;
}

Color GameRect::getOutlineColor()
{
	return outlineColor;
}

void GameRect::showColor()
{
	// w zale�no�ci czy wype�nieniem jest tekstura czy kolor
	if (shapeSprite == nullptr)
		RectangleShape::setFillColor(fillColor);
	else
		shapeSprite->setColor(fillColor);
}

void GameRect::hideRectColor()
{
	// w zale�no�ci czy wypelnieniem jest tekstura czy kolor
	if (shapeSprite == nullptr)
		RectangleShape::setFillColor(Color::Transparent);
	else
		shapeSprite->setColor(Color::White);
}

void GameRect::showOutline()
{
	this->setOutlineColor(outlineColor);
}

void GameRect::hideOutline()
{
	RectangleShape::setOutlineColor(Color::Transparent);
}

void GameRect::setTexture(const string& path, Space tankBody)
{
	auto lastRotation = space->getRotation();
	setRotation(0);
	hideRectColor(); // dla bez tekstury

	bool result = shapeTexture.loadFromFile(path);
	if (!result)
		printf("error obrazka");

	delete shapeSprite;
	shapeSprite = new Sprite(shapeTexture);

	// ustawienie �rodka dla obrot�w; ustawienie wymaga orginalnych rozmiarow 
	shapeSprite->setOrigin(tankBody.getXMid(), tankBody.getYMid());

	// przeskalowanie obrazka, wyswietlnie tankBody w srodku prostokatka, 'otoczenie' czo�gu na zawnatrz jest widoczne
	// do takich elementow nalezy np. lufa
	Vector2f scale = tankBody.scale(space->getWidth(), space->getHeight()); // skalar dopasowujacy do obecnego kwadratu
	shapeSprite->setScale(scale);
	setRotation(lastRotation);

	showColor(); // dla juz z tekstura
}

void GameRect::draw()
{
	// aktualizacja pozycji 
	RectangleShape::setPosition((space->getXMid()),(space->getYMid()));
	setRotation(getRotation());	// odswiezenie na wypadek zmian w Space

	auto window = engine->getGameWindow();
	window->draw(*this);

	if (shapeSprite != nullptr){
		window->draw(getAndUpdateShapeSprite());
	}
}

Sprite& GameRect::getAndUpdateShapeSprite()
{
	shapeSprite->setPosition(space->getXMid(), space->getYMid());
	//shapeSprite->setRotation(getRotation());
	return *shapeSprite;
}

float GameRect::getYMid()
{
	return space->getYMid();
}

Space& GameRect::getSpace()
{
	return *space;
}

