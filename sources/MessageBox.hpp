#pragma once
#ifndef _MESSAGEBOX_HPP_
#define _MESSAGEBOX_HPP_
#include <SFML/Graphics.hpp>
#include "Button.hpp"

class MessageBox
{
	friend class Button;
public:
	MessageBox(EngineClient* engine);
	
private:
	EngineClient* engine;

	Vector2u position;
	Vector2f size;

	// menu field
	RectangleShape menuBackground;
	Texture menuBackgroundTexture;
	String menuBackgroundPath;
	bool active;
	std::vector<Button*> buttons;




	// menu Text
	Text menuLabel;
	Font menuFont;
	int currentButton;
	bool visible;
};
#endif
