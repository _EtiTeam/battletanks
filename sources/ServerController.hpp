#pragma once
#ifndef _SERVER_CONTROLLER_HPP_
#define _SERVER_CONTROLLER_HPP_

#include "NetworkController.hpp"
#include "ConcurrentQueue.hpp"
#include "Space.hpp"

#include <atomic>
#include <map>

class EngineBase;
class GameUserEvents;

typedef unsigned short u_short;

using namespace std;

class ServerController : public NetworkController
{
public:
	ServerController(EngineBase* engine, u_short serverPort);
	virtual ~ServerController();
	
	ConcurrentQueue<Json::Value*>& getToSendQueue() override final;
	ConcurrentQueue<Json::Value*>& getToReceiveQueue() override final;

	// metoda dzialajaca w glownej petli gry
	// serializuje gre oraz odswieza GameEvents
	void update();

	// metody wykonywane za pomoca odrebnych watkow
	static DWORD WINAPI serviceSendingSocket(void* serviceCfg);
	static DWORD WINAPI serviceReceivingSocket(void* serviceCfg);

	const static u_short SERVER_PORT = 27016;

	const size_t TO_SEND_BUFFOR_SIZE = 10;	
	const size_t TO_RECV_BUFFOR_SIZE = 10;

protected:
	ConnectCfg serverConnectCfg;
	
private:
	atomic<size_t> nextId;
	static const Space spaces[MAX_CLIENTS];
	
	// przechowywanie zeserializowanego stanu gry do wyslania
	ConcurrentQueue<Json::Value*> toSend;
	// przechowywania zeserializowanego stanu GameEvents to zastosowania
	ConcurrentQueue<Json::Value*> toRecv;

	// spamietanie ostatniego GameUserEvent - na serwerze nie istnieje taki komponent
	map<size_t, GameUserEvents*> remoteGameUserEvents;
};

#endif