#include "EngineBase.hpp"
#include "Level1.hpp"
#include "Tank1.hpp"

#include <iostream>
#include <windows.h>

#define BORDO 105, 15, 40

const float EngineBase::SCREEN_WIDTH = 1050.0f;
const float EngineBase::SCREEN_HEIGHT = 650.0f;

EngineBase::EngineBase()
{
	InitializeCriticalSection(&criticalSection);
	objectManager = new ObjectManager(1, this);
	level = nullptr;

	setLevel(new Level1(this));
	gameClock.restart();
	lifeTimeClock.restart();
}

EngineBase::~EngineBase()
{
	Object* result;
	while (toDelete.tryPop(&result))
		delete result;

	delete objectManager;
	delete level;

	DeleteCriticalSection(&criticalSection);
}

void EngineBase::serialize(Json::Value& jsonDest)
{
	EnterCriticalSection(&criticalSection);
	objectManager->serialize(jsonDest[OBJECT_MANAGER]);
	LeaveCriticalSection(&criticalSection);
}

void EngineBase::serializeChanges(Json::Value& objectDest)
{
	EnterCriticalSection(&criticalSection);
	this->getObjectManager()->serializeChanges(objectDest);
	LeaveCriticalSection(&criticalSection);
}

void EngineBase::merge(Json::Value& jsonSrc)
{
	EnterCriticalSection(&criticalSection);
	objectManager->merge(jsonSrc[OBJECT_MANAGER]);
	LeaveCriticalSection(&criticalSection);
}

void EngineBase::mergeChanges(Json::Value& jsonObj)
{
	EnterCriticalSection(&criticalSection);
	this->getObjectManager()->mergeChanges(jsonObj);
	LeaveCriticalSection(&criticalSection);
}

void EngineBase::update()
{
	//Json::Value json;
	//objectManager->serialize(jsonValue);
	
	// pobranie czasu ktory uplynal dla wykonania akcji
	EnterCriticalSection(&criticalSection);

	gameDeltaTime = gameClock.getElapsedTime();
	gameClock.restart();
	objectManager->update();

	auto mainEvents = getMainGameUserEvents();
	mainEvents->update();
	
	//if (mainEvents->getBindedObject() != nullptr){
	//	getMainGameUserEvents()->serialize(json);
			//TODO //gameEventsToSend.push(json);
	//}
	Object* result;
	while (toDelete.tryPop(&result))
		delete result;

	LeaveCriticalSection(&criticalSection);
	//objectManager->merge(jsonValue);
}

Level* EngineBase::getLevel()
{
	EnterCriticalSection(&criticalSection);
	auto tmp = level;
	LeaveCriticalSection(&criticalSection);
	return tmp;
}

Time EngineBase::getLifeTime()
{
	EnterCriticalSection(&criticalSection);
	auto tmp = lifeTimeClock.getElapsedTime();
	LeaveCriticalSection(&criticalSection);
	return tmp;
}

Time EngineBase::getDeltaTime()
{
	EnterCriticalSection(&criticalSection);
	auto tmp = gameDeltaTime;
	LeaveCriticalSection(&criticalSection);
	return tmp;
}

ObjectManager* EngineBase::getObjectManager()
{
	//if (TryEnterCriticalSection(&criticalSection))
	//{
	//	throw exception();
	//}
	EnterCriticalSection(&criticalSection);
	auto objectManagerTmp = objectManager;
	LeaveCriticalSection(&criticalSection);
	return objectManagerTmp;
}

void EngineBase::addToDeleteInMainThread(Object** ptr)
{
	toDelete.push(ptr);
}

CRITICAL_SECTION* EngineBase::getCriticalSection()
{
	return &criticalSection;
}

void EngineBase::setLevel(Level* newLevel)
{
	EnterCriticalSection(&criticalSection);
	delete level;
	level = newLevel;
	LeaveCriticalSection(&criticalSection);
}

void EngineBase::setObjectManager(ObjectManager* other)
{
	EnterCriticalSection(&criticalSection);
	std::swap(objectManager, other);
	objectManager->applyBindAllGameEvent();
	LeaveCriticalSection(&criticalSection);
	
	// biezaca funkcja nie jest wywolywana tylko w watku glownym
	Object* a = other;
	addToDeleteInMainThread(&a);
}
