#include "Tank.hpp"
#include "GameUserEvents.hpp"
#include "EngineClient.hpp"
#include "Bullet.hpp"
#include <thread>
#include <SFML/System/Time.hpp>
#include <iostream>

Tank::Tank(size_t id, EngineBase* engine, Point& p, GameEvents* gameEvents)
	:
	DynamicObject(id, engine, Space(p.x, p.y, p.x + TANK_WIDTH, p.y + TANK_HEIGHT)),
	tankShape(engine, space),
	gameEvents(gameEvents)
{
	gameEventsOff();
	lives = 3;
	shootTime = clock();
}

// przypisanie bez gameEvents, wszystkie elelementy nie maja domyslnie gameEvents
Tank::Tank(EngineBase* engine, Json::Value& objectSrc)
	:
	DynamicObject(engine, objectSrc),
	tankShape(engine, space),
	gameEvents(nullptr),
	lives(objectSrc[TANK_LIVES].asInt())
{
	gameEventsOff();
	shootTime = clock();

	if (gameEvents != nullptr)
		gameEvents->setBindedObject(this);
}

Tank::~Tank()
{
	//unbindGameEvents();
}

void Tank::serialize(Json::Value& objectDest)
{
	objectDest[TANK_LIVES] = lives;
	DynamicObject::serialize(objectDest);
}

void Tank::merge(GameObject* gameObject)
{
	DynamicObject::merge(gameObject);

	auto object = reinterpret_cast<Tank*>(gameObject);
	lives = object->getLives();
}

bool Tank::isReloaded()
{
	float diff= static_cast<float>(clock() - shootTime) / CLOCKS_PER_SEC;
	if (diff > 1)
		return true;
	
	return false;
}

//Space& Tank::getSpace()
//{
//	return tankShape.getSpace();
//}
//
//void Tank::setSpace(Space& space)
//{
//	DynamicObject::setSpace(space);
//	tankShape.setPosition(space);
//}

int Tank::getZIndex()
{
	return 0;
}
void Tank::rotate(Direction direction)
{
	switch (direction){
		case LEFT: setRotation(180); break;
		case RIGHT:setRotation(0); break;
		case UP: setRotation(270); break;
		case DOWN: setRotation(90); break;
	}
}

GameRect* Tank::getGameRect()
{
	return &tankShape;
}

void Tank::gameEventsOn()
{
	isGameEventOn = true;
}

void Tank::gameEventsOff()
{
	isGameEventOn = false;
}

void Tank::bindGameEvents(GameEvents* gameEvents)
{
	this->gameEvents = gameEvents;
	gameEventsOn();
}

void Tank::applyBindGameEvents()
{
	if (gameEvents != nullptr){
		gameEvents->setBindedObject(this);
		gameEventsOn(); // dzialanie zgodnie z przekazanym gameEvents
	}
	else
		throw "a";
}

void Tank::unbindGameEvents()
{
	if (gameEvents){
		gameEvents->setBindedObject(nullptr);
		gameEvents = nullptr;
	}
	gameEventsOff();
}

void Tank::update()
{
	isReloaded();
	if (isGameEventOn && gameEvents != nullptr){
		Direction direction = gameEvents->getDirection();
		bool makeShoot = gameEvents->makeShoot();
		if (direction != NONE){
			rotate(direction);
			motion.speedUp(direction);
			engine->getObjectManager()->addObjectToSend(this);
		}
		if (makeShoot && isReloaded() && !isDestroyed()){
			shootTime = clock();
			Space space = this->getSpace();
			int angle = static_cast<int>(space.getRotation());			
			Bullet* newBullet = nullptr;

			switch (angle)
			{
				case 0 : //PRAWO
					newBullet = new Bullet(0, engine, Point(space.x2 + 50, space.getYMid()-5), RIGHT);
					break;
				case 180: //LEWO
					newBullet = new Bullet(0, engine, Point(space.x1 - 50, space.getYMid() - 5), LEFT);
					break;
				case 270: //G�RA
					newBullet = new Bullet(0, engine, Point(space.getXMid() - 5, space.y1-50), UP);
					break;
				case 90: //Dӳ
					newBullet = new Bullet(0, engine, Point(space.getXMid() - 5, space.y2+50), DOWN);
					break;
			}
			
			if(newBullet!= nullptr)
				engine->getObjectManager()->addObject(newBullet);
		}
	}
	DynamicObject::update(); // faktyczna realizacja ruchu
}

void Tank::update(GameEvents* gameEvents)
{
	this->gameEvents = gameEvents;
	auto dir = gameEvents->getDirection();
	gameEventsOn();
	//update(); // TODO
	
	//isReloaded();
	//Direction direction = gameEvents->getDirection();
	//bool makeShoot = gameEvents->makeShoot();
	//if (direction != NONE){
	//	rotate(direction);
	//	motion.speedUp(direction);
	//	engine->getObjectManager()->addObjectToSend(this);
	//}
	//if (makeShoot && isReloaded() && !isDestroyed()){
	//	shootTime = clock();
	//	Space space = this->getSpace();
	//	int angle = static_cast<int>(space.getRotation());
	//	Bullet* newBullet = nullptr;

	//	switch (angle)
	//	{
	//	case 0: //PRAWO
	//		newBullet = new Bullet(0, engine, Point(space.x2 + 50, space.getYMid() - 5), RIGHT);
	//		break;
	//	case 180: //LEWO
	//		newBullet = new Bullet(0, engine, Point(space.x1 - 50, space.getYMid() - 5), LEFT);
	//		break;
	//	case 270: //G�RA
	//		newBullet = new Bullet(0, engine, Point(space.getXMid() - 5, space.y1 - 50), UP);
	//		break;
	//	case 90: //Dӳ
	//		newBullet = new Bullet(0, engine, Point(space.getXMid() - 5, space.y2 + 50), DOWN);
	//		break;
	//	}

	//	if (newBullet != nullptr)
	//		engine->getObjectManager()->addObject(newBullet);
	//}
}

