#include "EngineServer.hpp"
#include "Tank1.hpp"
#include "ServerController.hpp"

EngineServer::EngineServer()
	:
	mainGameUserEvents(this, false),
	EngineBase()
{	
	serverController = new ServerController(this, ServerController::SERVER_PORT);
	//objectManager->addObject(new Tank1(0, this, Point(100, 100)));
	//auto a = new ObjectManager(2, this);
	//swap(objectManager, a);
	//Json::Value json;
	//a->serializeChanges(json);
	//objectManager->mergeChanges(json);
	
	//Json::Value jsonValue;
	//objectManager->serialize(jsonValue);
	//cout << jsonValue << endl;
	//delete objectManager;
	//objectManager = new ObjectManager(1, this);
	//objectManager->merge(jsonValue);
}

EngineServer::~EngineServer()
{
	delete serverController;
}

void EngineServer::update()
{
	// na koncu pozostale dane
	auto section = getCriticalSection();
	EnterCriticalSection(section);
	// na koncu serializacja poprzez serverEngine, jesli zainicjalizowano
	// odswiezenie musi nastapic przed update() - odswiezenie stanu GameEvent
	if (serverController != nullptr)
		serverController->update();
	EngineBase::update();

	LeaveCriticalSection(section);
}

GameEvents* EngineServer::getMainGameUserEvents()
{
	return  &mainGameUserEvents; 
}
