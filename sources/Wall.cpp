#include "Wall.hpp"
#include "EngineClient.hpp"
using namespace sf;

Wall::Wall(size_t id, EngineBase* engine, Point& p)
	:
	StaticObject(id, engine, Space(p.x, p.y, p.x + Wall_WIDTH, p.y + Wall_HEIGHT)),
	wallShape(engine, space)
{
	wallShape.setTexture("images/wall.png", Space(0, 0, 20, 20));
	wallShape.hideRectColor();
	wallShape.hideOutline();
	this->lives = 3;
}

Wall::Wall(EngineBase* engine, Json::Value& objectSrc)
	:
	StaticObject(engine, objectSrc),
	wallShape(engine, space)
{
	wallShape.setTexture("images/wall.png", Space(0, 0, 20, 20));
	wallShape.hideRectColor();
	wallShape.hideOutline();
	this->lives = 3;
}

void Wall::update()
{
	StaticObject::update();
}

void Wall::draw()
{
	
	if (lives < 0){
		this->setTransparet();
		
	}
	else{
		if (lives == 2) wallShape.setTexture("images/wall2.png", Space(0, 0, 20, 20));
		if (lives == 1) wallShape.setTexture("images/wall3.png", Space(0, 0, 20, 20));
		if (lives == 0) wallShape.setTexture("images/wall4.png", Space(0, 0, 20, 20));
		wallShape.hideRectColor();
		wallShape.hideOutline();
		wallShape.draw();
	}
	//RenderWindow& gameWindow = *engine->getGameWindow();
	//sf::CircleShape ksztalt(gameWindow.getSize().y / 4);
	//ksztalt.setOrigin(Vector2f(ksztalt.getRadius(), ksztalt.getRadius()));
	//ksztalt.setPosition((gameWindow.getSize().x) / 2.0f + przesuniecieX, (gameWindow.getSize().y) / 2.0f);
	//ksztalt.setFillColor(Color::Yellow);
}

int Wall::getZIndex()
{
	return 0;
}

ObjectType Wall::getObjectType()
{
	return WALL;
}

CanMove Wall::collision(GameObject* withObject)
{
	if (withObject->getObjectType() == BULLET)
		this->lives--;
	return NO_CANT;
}

int Wall::getLives()
{
	return this->lives;
}
