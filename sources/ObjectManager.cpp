#include "Tank.hpp"
#include "Level.hpp"
#include "EmptyObject.hpp"
#include "EngineClient.hpp"
#include "DynamicObject.hpp"
#include "ObjectManager.hpp"
#include "otherFunctions.hpp"
#include "GameUserEvents.hpp"

#include <vector>
#include <algorithm>
#include <iostream>

//using namespace sf;
using std::vector;
using std::sort;

ObjectManager::ObjectManager(size_t id, EngineBase* engine)
	: objectSet(engine)
{
	this->objectToRemoveSet = new GameObjectSet(engine);
	this->engine = engine;
	this->id = id;
	this->objectCounter = 0;
	InitializeCriticalSection(&criticalSection);
}

ObjectManager::~ObjectManager()
{	
	delete objectToRemoveSet;
	DeleteCriticalSection(&criticalSection);
}

CRITICAL_SECTION* ObjectManager::getCriticalSection()
{
	return &criticalSection;
}

void ObjectManager::serialize(Json::Value& objectDest)
{
	EnterCriticalSection(&criticalSection);
	objectSet.serialize(objectDest[OBJECT_SET]);

	Json::Value& objectToRemoveSet = objectDest[OBJECT_TO_REMOVE_SET];
	auto i = 0;
	for (auto it = idToRemove.begin(); it != idToRemove.end(); ++it, ++i)
	{
		auto id = (*it);
		objectToRemoveSet[i] = id;
	}
	LeaveCriticalSection(&criticalSection);
}

void ObjectManager::merge(Json::Value& jsonObj)
{

	//cout << jsonObj << endl;
	EmptyObject emptyObject(0);

	EnterCriticalSection(&criticalSection);
	objectSet.merge(jsonObj[OBJECT_SET]);

	auto objectToRemove = jsonObj[OBJECT_TO_REMOVE_SET];
	for (auto it = objectToRemove.begin(); it != objectToRemove.end(); ++it)
	{
		auto id = (*it).asLargestUInt();
		//emptyObject.setId(id);
		//auto itFinded = objectSet.find(&emptyObject);
		auto obj = getObject(id);
		
		//objectToSendSet.erase(&emptyObject);
		//if (itFinded != objectSet.end()){
		if (obj != nullptr)
			deleteObject(obj);
	}
	LeaveCriticalSection(&criticalSection);
}

void ObjectManager::serializeChanges(Json::Value& objectDest)
{
	EnterCriticalSection(&criticalSection);

	objectToSendSet.serialize(objectDest[OBJECT_SET]);
	objectToSendSet.clear();	// obiekty juz wyslane

	LeaveCriticalSection(&criticalSection);	
}

void ObjectManager::mergeChanges(Json::Value& jsonObj)
{
	EnterCriticalSection(&criticalSection);
	objectSet.merge(jsonObj[OBJECT_SET]);
	LeaveCriticalSection(&criticalSection);
}

//void ObjectManager::mergeMovement(GameUserEvents& gameUserEvent)
//{
//	EnterCriticalSection(&criticalSection);
//
//	LeaveCriticalSection(&criticalSection);
//}


//
//void ObjectManager::mergeMovement(Json::Value& jsonObj, void* wsk)
//{
//	// TODO zmiana na getObject
//	size_t id = jsonObj["bindedObjectId"].asLargestUInt();
//	EmptyObject objToFind(id);
//	 = new GameUserEvents(jsonObj);
//
//	EnterCriticalSection(&criticalSection);
//	auto it = objectSet.find(&objToFind);
//	if (it != objectSet.end())
//	{
//		auto obj = reinterpret_cast<DynamicObject*>(*it);
//		obj->update(userEvent);
//	}
//	delete wsk;
//	wsk = userEvent; // spamietanie aby nastepnym razem mozna bylo usunac
//
//	LeaveCriticalSection(&criticalSection);
//}

void ObjectManager::update()
{
	// TODO do wektora posortowanego 
	EnterCriticalSection(&criticalSection);

	auto staticObjects = engine->getLevel()->getLevelStaticObjects();
	for (auto it = staticObjects->begin(); it != staticObjects->end(); ++it)
	{
		(*it)->update();
	}

	for (auto it = objectSet.begin(); it != objectSet.end(); ++it)
	{
		(*it)->update();
	}

	for (auto it = objectToRemoveSet->begin(); it != objectToRemoveSet->end(); ++it)
	{
		objectSet.erase(*it);
		objectToSendSet.erase(*it);
	}
	objectToRemoveSet->clear(); // USUNIECIE OBIEKTOW OZNACZONYCH DO USUNIECIA

	LeaveCriticalSection(&criticalSection);
}

GameObject* ObjectManager::getObject(size_t id)
{
	EnterCriticalSection(&criticalSection);
	EmptyObject emptyObject(id);
	auto it = objectSet.find(&emptyObject);
	if (it == objectSet.end()){
		LeaveCriticalSection(&criticalSection);
		return nullptr;
	}
	LeaveCriticalSection(&criticalSection);
	return *it;
}

void ObjectManager::addObject(GameObject* newObject)
{
	EnterCriticalSection(&criticalSection);

	auto tmp = objectCounter.fetch_add(1);
	if (tmp >= MAX_ELEMENTS)
		throw "zbyt duza ilosc obiektow";
	size_t id = tmp + this->id * MAX_ELEMENTS;	// generowanie id z przedzialu dla danego gameObject
															// niezbedne dla prawidlowego dzialania sieci
	newObject->id = id;

	objectSet.insert(newObject);
	objectToSendSet.insert(newObject);

	LeaveCriticalSection(&criticalSection);
}

void ObjectManager::addObjectToSend(GameObject* object)
{
	EnterCriticalSection(&criticalSection);
	objectToSendSet.insert(object);
	LeaveCriticalSection(&criticalSection);
}

void ObjectManager::deleteObject(GameObject* toRemove)
{
	EnterCriticalSection(&criticalSection);

	idToRemove.insert(toRemove->getId());
	objectToRemoveSet->insert(toRemove);		// TODO sprawdzic czy dziala
	objectToSendSet.erase(toRemove);

	LeaveCriticalSection(&criticalSection);
}

GameObject* ObjectManager::isCollision(GameObject& sourceGameObject, Space& newSpace)
{
	EnterCriticalSection(&criticalSection);

	auto collisionWithObject = ObjectManager::isCollision(objectSet, sourceGameObject, newSpace);
	if (collisionWithObject != nullptr){
		LeaveCriticalSection(&criticalSection);
		return collisionWithObject;
	}

	// sprawdzenie czy nie ma nieprzezroczystego obiektu przezroczystego
	//auto gameObjects = reinterpret_cast<list<GameObject*>&>(*getStaticObjectList());
	auto gameObjects = engine->getLevel()->getLevelStaticObjects();

	auto tmp = ObjectManager::isCollision(*gameObjects, sourceGameObject, newSpace);

	LeaveCriticalSection(&criticalSection);
	return tmp;
}

GameObject* ObjectManager::isCollision(GameObjectSet& objectSet, GameObject& sourceGameObject, Space& newSpace)
{
	// obiekt przezroczysty nie moze sie z niczym zderzyc
	if (sourceGameObject.isTransparent())
		return nullptr;

	auto rect = newSpace.toIntRect();
	bool intersect;
	for (auto it = objectSet.begin(); it != objectSet.end(); ++it)
	{
		// unikniecie sprawdzenia samego siebie
		if (*it == &sourceGameObject) 
			continue;

		if ((*it)->isTransparent() == false)
		{
			// sprawdzenie przeciecia
			intersect = (*it)->getSpace().toIntRect().intersects(rect); 
			if (intersect)
				return *it;
		}
	}
	return nullptr; // brak przeciecia
}

size_t ObjectManager::getId()
{
	EnterCriticalSection(&criticalSection);
	auto tmp = this->id;
	LeaveCriticalSection(&criticalSection);
	return tmp;
}

//GameObjectSet* ObjectManager::getObjectSet()
//{
//	EnterCriticalSection(&criticalSection);
//	auto tmp = &objectSet;
//	LeaveCriticalSection(&criticalSection);
//	return tmp;
//}

//
//GameObjectSet* ObjectManager::getStaticObjectList()
//{
//	auto actualLevel = engine->getLevel();
//	if (actualLevel == nullptr)
//		throw "Level has not been loaded";
//	return actualLevel->getLevelStaticObjects();
//}

void ObjectManager::gameEventsOn()
{
	EnterCriticalSection(&criticalSection);

	for (auto it = objectSet.begin(); it != objectSet.end(); ++it){
		auto dynamicObj = dynamic_cast<DynamicObject*>(*it);
		dynamicObj->gameEventsOn();
	}

	LeaveCriticalSection(&criticalSection);
}

void ObjectManager::gameEventsOff()
{
	EnterCriticalSection(&criticalSection);
	
	for (auto it = objectSet.begin(); it != objectSet.end(); ++it){
		auto dynamicObj = dynamic_cast<DynamicObject*>(*it);
		dynamicObj->gameEventsOff();
	}

	LeaveCriticalSection(&criticalSection);
}

void ObjectManager::applyBindAllGameEvent()
{
	EnterCriticalSection(&criticalSection);
	for (auto it = objectSet.begin(); it != objectSet.end(); ++it)
	{
		(*it)->applyBindGameEvents();
	}
	LeaveCriticalSection(&criticalSection);
}

void ObjectManager::unbindAllGameEvent()
{
	EnterCriticalSection(&criticalSection);
	for (auto it = objectSet.begin(); it != objectSet.end(); ++it)
	{
		(*it)->unbindGameEvents();
	}
	LeaveCriticalSection(&criticalSection);
}

bool ObjectManager::isAvaible(Space& space)
{
	EnterCriticalSection(&criticalSection);
	auto level = engine->getLevel();
	auto tmp = level->isAvailable(space);
	LeaveCriticalSection(&criticalSection);
	return tmp;
}

void ObjectManager::draw()
{
	EnterCriticalSection(&criticalSection);
	
	auto staticObjects = engine->getLevel()->getLevelStaticObjects();
	
	static vector<GameObject*> allObjects;
	allObjects.clear();
	allObjects.insert(allObjects.end(), objectSet.begin(), objectSet.end());
	allObjects.insert(allObjects.end(), staticObjects->begin(), staticObjects->end());
	
	LeaveCriticalSection(&criticalSection);

	sort(allObjects.begin(), allObjects.end(), Comparator());

	for (auto it = allObjects.begin(); it != allObjects.end(); ++it)
	{
		(*it)->draw();
	}
}
