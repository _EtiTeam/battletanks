#pragma once
#ifndef _GAME_RECT_HPP_
#define _GAME_RECT_HPP_

#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include "Space.hpp"
#include "EngineClient.hpp"

#define STANDARD_THICK 1.0

using std::string;
using sf::RectangleShape;
using sf::Color;
using sf::Sprite;
using sf::Texture;
using sf::RenderStates;
using sf::RenderTarget;
using sf::Shape;

class GameRect 
	: private RectangleShape
{
public:
	// pierwszy z konstukotow wlaczony na rzecz poloczenie space z obiektami (referencja)
	//GameRect(EngineClient* engine, int xMiddle, int yMiddle, int width = STANDARD_WIDTH, 
	//	int height = STANDARD_HEIGHT, int tick = STANDARD_THICK); 
	GameRect(EngineBase* engine, Space& space, float thick = STANDARD_THICK);
	virtual ~GameRect();

	float getXMid();
	float getYMid();
	Space& getSpace();
	void move(float x, float y);
	void setPosition(Space& space);

	float getRotation();
	void setRotation(float angle);
	void setFillColor(Color color);
	void setOutlineColor(Color color);
	Color getFillColor();
	Color getOutlineColor();

	void showColor();
	void hideRectColor();
	void showOutline();
	void hideOutline();
	void setTexture(const string& path, Space tankBody);
	//void setTexture(string& path, Space tankBody);

	void draw();

	//static const unsigned int STANDARD_WIDTH = 80;
	//static const unsigned int STANDARD_HEIGHT = 80;

protected:
	Sprite& GameRect::getAndUpdateShapeSprite();

private:
	EngineBase* engine;

	Space *space; // dziala na orginale; nie usuwac tej zmiennej
	Color fillColor;
	Color outlineColor;

	Sprite* shapeSprite;
	Texture shapeTexture;
};

#endif