#pragma once
#ifndef _TANK_HPP_
#define _TANK_HPP_

#include "DynamicObject.hpp"
#include "GameRect.hpp"
#include "Point.hpp"

// postac gracza
class Tank abstract
	: public DynamicObject
{
public:
	Tank(size_t id, EngineBase* engine, Point& startLRPoint, GameEvents* gameEvents);
	Tank(EngineBase* engine, Json::Value& objectSrc);
	virtual ~Tank();

	void serialize(Json::Value& objectDest) override;
	void merge(GameObject* gameObject) override;
	bool isReloaded();

	void update() override final;
	void update(GameEvents* gameEvents) override final;

	int getZIndex() override;
	virtual bool isDestroyed() = 0 ;

	// metody zbedne gdyz GameRect jest polaczony z shape wbudowany w klase
	//virtual Space& getSpace() override;
	//virtual void setSpace(Space& space) override;

	//virtual CanMove collision(GameObject* withObject) override;

	void rotate(Direction direction);
	GameRect* getGameRect() override;

	void gameEventsOn() override;
	void gameEventsOff() override;
	void unbindGameEvents() override;
	void bindGameEvents(GameEvents* gameEvents); // po dodatniu domyslnie zostaje wywo�ana GameEventsOn
	void applyBindGameEvents() override final;

	static void reload(Tank* myTank);
	///static void reload();
protected:
	GameRect tankShape;

	bool isGameEventOn;
	GameEvents* gameEvents;
	int lives;
	//bool reloaded;
	const static int TANK_WIDTH = 150;
	const static int TANK_HEIGHT = 100;
	clock_t shootTime;
	
};

#endif