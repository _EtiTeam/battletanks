#pragma once
#ifndef _CLIENT_CONTROLLER_HPP_
#define _CLIENT_CONTROLLER_HPP_

#include "NetworkController.hpp"
#include "ConcurrentQueue.hpp"

class EngineBase;

using namespace std;

class ClientController : public NetworkController
{
public:
	ClientController(EngineBase* engine, const string& serverAddr, u_short serverPort);
	virtual ~ClientController() {}
	
	ConcurrentQueue<Json::Value*>& getToSendQueue() override final;
	ConcurrentQueue<Json::Value*>& getToReceiveQueue() override final;

	void start();
	void update();
	bool isStarted();
	
	// metody wykonywane za pomoca odrebnych watkow
	static DWORD WINAPI serviceSendingSocket(void* serviceCfg);
	static DWORD WINAPI serviceReceivingSocket(void* serviceCfg);
	
	const size_t TO_SEND_BUFFOR_SIZE = 4;	// odczucie opoznienie sterowania
	const size_t TO_RECV_BUFFOR_SIZE = 4;

private:
	bool started;
	ConnectCfg clientConnectCfg[MAX_CLIENTS];

	// przechowywanie zeserializowanego stanu gry do wyslania
	ConcurrentQueue<Json::Value*> toSend;
	// przechowywania zeserializowanego stanu GameEvents to zastosowania
	ConcurrentQueue<Json::Value*> toRecv;
};

#endif