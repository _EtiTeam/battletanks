#pragma once
#ifndef _GAME_CAMERA_HPP_
#define _GAME_CAMERA_HPP_

#include "Space.hpp"
#include "GameObject.hpp"

class EngineClient;

class GameCamera
{
public:
	GameCamera(EngineClient* engine, GameObject* followed);
	void setFollowed(GameObject* followed);

	// zwraca polozenie kamery wraz z polem widzenia
	// uwzglednia polozenie postaci przy krawedzi
	// jesli nullptr to srodek mapy
	Space getSpace(); 
private:
	EngineClient* engine;
	GameObject* followed;
};

#endif