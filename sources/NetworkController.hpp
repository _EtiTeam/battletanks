#pragma once
#ifndef _NETWORK_CONTROLLER_HPP_
#define _NETWORK_CONTROLLER_HPP_

#undef WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <vector>
#include "ConcurrentQueue.hpp"
#include "Object.hpp"

typedef UINT_PTR SOCKET;

namespace Json
{
	class Value;
}

enum ClientType
{
	RECEIVE = 0,
	SENDING = 1
};

class EngineBase;

using namespace std;

class NetworkController abstract : public Object
{
protected:
	// struktura do uzywania jako statyczna
	class WSAInitalizer
	{
	public:
		WSAInitalizer();
		~WSAInitalizer();
	};

	struct ConnectCfg
	{
		string addr;
		unsigned short port;
		EngineBase* engine;
		LPTHREAD_START_ROUTINE servicingFunction;
		NetworkController* networkController;
		ClientType type;
	};
	struct ServiceCfg
	{
		EngineBase* engine;
		SOCKET socket;
		NetworkController* networkController;
	};
	struct ConnectionException : exception
	{
		ConnectionException(const char* reason);
		ConnectionException(const char* reason, int nr);
	};

public:
	NetworkController(EngineBase* engine);
	virtual ~NetworkController();

	virtual ConcurrentQueue<Json::Value*>& getToSendQueue() = 0;
	virtual ConcurrentQueue<Json::Value*>& getToReceiveQueue() = 0;

	const static size_t BUFF_SIZE = 2000;

	// funkcja spamietujaca id watkow, aby je w czasie usuwania wylaczyc
	void addThreadHandle(HANDLE threadId);

	// metody wykonywane za pomoca odrebnych watkow
	static DWORD WINAPI startServer(void* connectCfg);
	static DWORD WINAPI startClient(void* connectCfg);

	// buff to buffor, przekazywany aby uniknac nadmiarowych alokacji
	static void recv(SOCKET& socket, Json::Value& jsonDest, char * buffChar, string& buffStr);
	static void send(SOCKET& socket, Json::Value& jsonToSend);
	
	const static size_t MAX_CLIENTS = 4;
protected:
	EngineBase* engine;

private:
	// do zmiennej odnosic sie tylko poprzez settery i gettery
	vector<HANDLE> threadHandles;
	CRITICAL_SECTION criticalSection;

	static WSAInitalizer WSAInitalizer_; // automatyczna inicjalizaca i usuniecie

};

#endif