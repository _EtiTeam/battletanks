#include "GameUserEvents2.hpp"
#include "EngineClient.hpp"

using namespace sf;

GameUserEvents2::GameUserEvents2(EngineBase* engine, bool readAllKey)
	: GameEvents(engine, readAllKey)
{
	direction = NONE;
}

GameUserEvents2::~GameUserEvents2()
{
	if (bindedObject != nullptr)
		bindedObject->unbindGameEvents();
}

void GameUserEvents2::update()
{
	if (Keyboard::isKeyPressed(Keyboard::D)) direction = RIGHT;
	else if (Keyboard::isKeyPressed(Keyboard::A)) direction = LEFT;
	else if (Keyboard::isKeyPressed(Keyboard::W)) direction = UP;
	else if (Keyboard::isKeyPressed(Keyboard::S)) direction = DOWN;
	else direction = NONE;
	if (Keyboard::isKeyPressed(Keyboard::Tab)) shoot = true;
	GameEvents::update(); // ewentualne wczytanie gdy flaga readAllKey == true
}

Direction GameUserEvents2::getDirection()
{
	return direction;
}
bool GameUserEvents2::makeShoot()
{
	bool makeShoot = shoot;

	shoot = false; //oddano strza�
	return makeShoot;
}

GameObject* GameUserEvents2::getBindedObject()
{
	return this->bindedObject;
}

void GameUserEvents2::setBindedObject(GameObject* obj)
{
	this->bindedObject = obj;
}