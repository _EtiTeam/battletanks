#pragma once
#ifndef _OBJECT_MANAGER_HPP
#define _OBJECT_MANAGER_HPP

#include "StaticObject.hpp"
#include "GameObjectSet.hpp"
#include "Object.hpp"

#include <list>
#include <atomic>

class GameUserEvents;
using std::list;

class EngineBase;
class Level;

class ObjectManager : public Object
{
public:
	ObjectManager(size_t id, EngineBase* engineBase);
	virtual ~ObjectManager();

	CRITICAL_SECTION* getCriticalSection();

	// wykorzystywane przy sieciowych zastosowaniach
	// serializacjia serwer->klient
	void serialize(Json::Value& objectDest);	 
	void merge(Json::Value& jsonObj);		

	// serializacja klient->serwer
	// dla obiekow w objectToSendSet
	void serializeChanges(Json::Value& objectDest);
	void mergeChanges(Json::Value& jsonObj);
	//void mergeMovement(Json::Value& jsonObj, void* wsk);
	//void mergeMovement(GameUserEvents& gameUserEvent);
	void draw();
	void update();

	GameObject* getObject(size_t id);
	void addObject(GameObject* newObject);		// kazdy dodany element zostanie automatycznie usuniety podczas usuwania objectManager
	void addObjectToSend(GameObject* object);
	void deleteObject(GameObject* toRemove);

	//bool isEmpty(Space& space);
	//GameObjectSet* getStaticObjectList();		// zaimplementowane z rzucaniem wyjatkow
	virtual void gameEventsOn();
	virtual void gameEventsOff();
	// metoda uzywana po utworzoniu GameManagera i uruchomieniu go
	// uruchomienie powiazan w gameUserEventach
	void applyBindAllGameEvent();
	void unbindAllGameEvent();
	
	bool isAvaible(Space& space);				// na podstawie tylko elementow statycznych lvl oraz rozmiaru lvl
	GameObject* isCollision(GameObject& sourceGameObject, Space& newSpace);
	static GameObject* isCollision(GameObjectSet& objectSet, GameObject& sourceGameObject, Space& newSpace);	// zwraca obiekt z ktorym jest kolizja lub nullptr

	const size_t MAX_ELEMENTS = 10000;
	size_t getId();
	//GameObjectSet* getObjectSet();
private:
	CRITICAL_SECTION criticalSection;
	atomic<size_t> objectCounter;	// ilosc przydzielonych juz id dla obiektow, wliczone sa nawet juz nieistniejace
	size_t id;						// id na postawie ktorego generowane sa wszystkie id obiektow tworzonych 
									// generacja dochodzi przy wywolaniu addObject(GameObject* obj)
	EngineBase* engine;
	GameObjectSet* objectToRemoveSet;	// obiekty ktore zostaly oznaczone jako do usuniecia przez deleteObject
	GameObjectSet objectSet;	//	zawiera elementy tylko dodane poprzez addObject
								//	nie zawiera elementow statycznych dostarczanych przez level
								//	nie usuwac elementow, mapa posiada destruktor ktora usuwa GameObject
	// struktury do funkcjonowania sieci
	GameObjectSet_notAutoDeletedAndMerged objectToSendSet;
	unordered_set<size_t> idToRemove;	// set serializowany
};


#endif
