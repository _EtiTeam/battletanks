#pragma once
#ifndef _ENGINE_SERVER_HPP_
#define _ENGINE_SERVER_HPP_

#include "GameUserEvents.hpp"
#include "EngineBase.hpp"

#include <SFML/Graphics.hpp>

using namespace sf;

class ServerController;

class EngineServer 
	: public EngineBase
{
public:
	EngineServer();
	virtual ~EngineServer();

	void draw() override	{}
	void update() override;
	bool isOpen() override	{ return true; } 

	GameCamera* getGameCamera() override				{ return nullptr; }
	RenderWindow* getGameWindow() override				{ return nullptr; }
	GameEvents* getMainGameUserEvents() override;
	vector<GameEvents*>* getOtherGameEvents() override  { return nullptr; }

private:	
	GameUserEvents mainGameUserEvents;	//v TODO zmienic na cos kosolowego
	ServerController* serverController;
};

#endif