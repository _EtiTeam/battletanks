#include "EmptyObject.hpp"

EmptyObject::EmptyObject(size_t id)
	: GameObject(id, nullptr, Space(0,0,0,0))
{
}

void EmptyObject::setId(size_t id)
{
	GameObject::setId(id);
}
