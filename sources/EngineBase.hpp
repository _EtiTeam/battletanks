#pragma once
#ifndef _ENGINE_BASE_HPP_
#define _ENGINE_BASE_HPP_

#include "GameCamera.hpp"
#include "GameEvents.hpp"
#include "ObjectManager.hpp"
#include "ConcurrentQueue.hpp"
#include "Object.hpp"

#include <vector>
#include <windows.h>
#include <SFML/Graphics.hpp>


namespace Json
{
	class Value;
}

using namespace sf;
using std::vector;
using std::for_each;

class EngineBase abstract : public Object
{
public:
	EngineBase();
	virtual ~EngineBase();

	// wykorzystywane przy sieciowych zastosowaniach
	void serialize(Json::Value& jsonDest);
	void serializeChanges(Json::Value& objectDest);
	void merge(Json::Value& jsonSrc);
	void mergeChanges(Json::Value& jsonObj);
	virtual void update();
	virtual void draw() = 0;

	virtual bool isOpen() = 0;

	virtual Level* getLevel() final;
	virtual Time getLifeTime() final;
	virtual Time getDeltaTime() final;
	virtual void setLevel(Level* newLevel) final;	// nie usuwac elementu sie przekazalo
	void setObjectManager(ObjectManager* other);
	virtual ObjectManager* getObjectManager() final;
	//GameCamera* getGameCamera();

	virtual GameCamera* getGameCamera() = 0;
	virtual RenderWindow* getGameWindow() = 0;
	virtual GameEvents* getMainGameUserEvents() = 0;
	virtual vector<GameEvents*>* getOtherGameEvents() = 0;

	void addToDeleteInMainThread(Object** ptr);
	CRITICAL_SECTION* getCriticalSection();
	//ConcurrentQueue<Json::Value>* getGameEventsToSend();

	const static float SCREEN_WIDTH;
	const static float SCREEN_HEIGHT;
	
protected:
	Clock lifeTimeClock;
	Clock gameClock;
	Time gameDeltaTime;
	
	Level* level;
	ObjectManager* objectManager;
private:
	// struktura do eliminacji bedow usuwania przez inne watki
	ConcurrentQueue<Object*> toDelete;
	CRITICAL_SECTION criticalSection;
};

#endif