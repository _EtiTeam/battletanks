#include "GameUserEvents.hpp"
#include "EngineClient.hpp"
#include "otherFunctions.hpp"

using namespace sf;

GameUserEvents::GameUserEvents(EngineBase* engine, bool readAllKey)
	: GameEvents(engine, readAllKey)
{
	direction = NONE;
	bindedObject = nullptr;
	shoot = false;
}

GameUserEvents::GameUserEvents(Json::Value& json)
	: GameEvents(json)
{
	direction = static_cast<Direction>(json["direction"].asInt());
	bindedObject = nullptr;
	shoot = json["shoot"].asBool();
}

GameUserEvents::~GameUserEvents()
{
	if (bindedObject != nullptr)
		bindedObject->unbindGameEvents();
}

void GameUserEvents::serialize(Json::Value& json)
{
	GameEvents::serialize(json);
	json[DIRECTION] = direction;
	json[ID_BINDED_OBJECT] = bindedObject->getId();
	json[SHOOT] = shoot;
}

void GameUserEvents::update()
{
	auto window = engine->getGameWindow();
	if (isFocus(window)){
		if (Keyboard::isKeyPressed(Keyboard::Right)) direction = RIGHT;
		else if (Keyboard::isKeyPressed(Keyboard::Left)) direction = LEFT;
		else if (Keyboard::isKeyPressed(Keyboard::Up)) direction = UP;
		else if (Keyboard::isKeyPressed(Keyboard::Down)) direction = DOWN;
		else direction = NONE;

		if (Keyboard::isKeyPressed(Keyboard::Space)) shoot = true;
		else shoot = false;
	}

	GameEvents::update();
}

void GameUserEvents::update(Json::Value& json)
{
	direction = static_cast<Direction>(json[DIRECTION].asInt());
	shoot = json[SHOOT].asBool();

	GameEvents::update(json);
}

Direction GameUserEvents::getDirection()
{
	return direction;
}

GameObject* GameUserEvents::getBindedObject()
{
	return this->bindedObject;
}

void GameUserEvents::setBindedObject(GameObject* obj)
{
	this->bindedObject = obj;
}

bool GameUserEvents::makeShoot()
{
	bool makeShoot = shoot;
	
	shoot = false; //oddano strza�
	return makeShoot;
}

