#pragma once
#ifndef _SPACE_HPP_
#define _SPACE_HPP_
#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/Rect.hpp>

namespace Json
{
	class Value;
}

struct Point;
using sf::Vector2f;
using sf::IntRect;
using sf::FloatRect;
using sf::Rect;

class Space
{
public:
	Space(float x1, float y1, float x2, float y2);
	Space(int x1, int y1, int x2, int y2);
	Space(int xMiddel, int yMiddle, int width, int height, bool /*nothing*/);
	Space(Json::Value& objectSrc);

	void serialize(Json::Value& objectDest) const;

	Vector2f scale(float destWidth, float destHeight); // metoda zwraca vektor ktorym obiekt zostal przeskalowany
	float getWidth() const;
	float getHeight() const;

	// srodkowe pkt przestrzeni
	float getXMid(); 
	float getYMid();

	void move(float x, float y);
	// ustawienie wraz z nadpisaniem kata obrotu
	void setPosition(float x1, float y1, float x2, float y2);
	void setPosition(Space& space);
	void setPosition(Point& centerPoint);

	float getRotation();
	void setRotation(float angle);

	// konwersja
	template<typename T>
	void fromRect(Rect<T> rect);
	IntRect toIntRect() const;
	FloatRect toFloatRect() const;

	// dostep bezposredni
	float x1;
	float y1;
	float x2;
	float y2;
private:
	// zmienna, ktorej zmiany nalezy dokonywac wylacznie poprzez:
	// setRotation lub setPosition
	float rotationAngle;
};

template <typename T>
void Space::fromRect(Rect<T> rect)
{
	x1 = static_cast<float>(rect.left);
	y1 = static_cast<float>(rect.top);
	x2 = x1 + static_cast<float>(rect.width);
	y2 = y1 + static_cast<float>(rect.height);
}

#endif
