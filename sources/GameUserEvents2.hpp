#pragma once
#ifndef _GAME_USER_EVENTS2_HPP_
#define _GAME_USER_EVENTS2_HPP_

#include "GameEvents.hpp"
class GameUserEvents2 : public GameEvents
{
public:
	GameUserEvents2(EngineBase* engine, bool readAllKey = false);
	virtual ~GameUserEvents2();

	void update() override; // odbieranie wszystkich zdarzen
	GameObject* getBindedObject() override final;
	void setBindedObject(GameObject* obj) override final;
	Direction getDirection() override;
	bool makeShoot() override final;
private:
	Direction direction;
	GameObject* bindedObject;
	bool shoot;
};

#endif