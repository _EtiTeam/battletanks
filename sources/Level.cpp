#include "Level.hpp"
#include "ObjectManager.hpp"
#include "Wall.hpp"

Level::Level(EngineBase* engine)
	:
	staticObjects(engine)
{
	this->engine = engine;
	backgroundColor = sf::Color(125, 125, 125);
	idNumeration = 5678;
}

Level::~Level()
{
}

GameObjectSet* Level::getLevelStaticObjects()
{
	return &staticObjects;
}

void Level::setBackground(string path)
{
	bool result = backgroundTexture.loadFromFile(path);
	if (!result)
		printf("error obrazka");

	backgroundTexture.setRepeated(true);
	backgroundSprite.setTexture(backgroundTexture);
	backgroundSprite.setTextureRect(getLevelSpace().toIntRect());
}

void Level::drawLineHorizontal(float x1, float x2, float y )
{
	for (float tmp = x1; tmp <= x2; tmp+=20)
		staticObjects.insert(new Wall(idNumeration++, engine, Point(tmp, y)));
	
}

void Level::drawLineVertical(float y1, float y2, float x)
{
	for (float tmp = y1; tmp <= y2; tmp += 20)
		staticObjects.insert(new Wall(idNumeration++, engine, Point(x, tmp)));
}

bool Level::isAvailable(Space& space)
{
	// sprawdzenie czy jest w polu
	auto lvlRect = getLevelSpace().toFloatRect();
	if (lvlRect.contains(space.x1, space.y1) == false) 
		return false;
	if (lvlRect.contains(space.x1, space.y2) == false) 
		return false;
	if (lvlRect.contains(space.x2, space.y1) == false) 
		return false;
	if (lvlRect.contains(space.x2, space.y2) == false) 
		return false;

	return true;
}

Color Level::getBackgroundColor()
{
	return backgroundColor;
}

Sprite& Level::getBackroundSprite()
{
	return backgroundSprite;
}
