#include "Bullet.hpp"
#include "GameUserEvents.hpp"
#include "EngineClient.hpp"

Bullet::Bullet(size_t id, EngineBase* engine, Point& p, Direction direction)
	:
	DynamicObject(id, engine, Space(p.x, p.y, p.x + Bullet_WIDTH, p.y + Bullet_HEIGHT)),
	bulletShape(engine, space)
{
	this->motion.setSpeedProperty({ 0, 0, 1000, 1000 });
	switch (direction)
	{
	case LEFT:
		this->motion.speedUp(LEFT, false); break;
	case RIGHT:
		this->motion.speedUp(RIGHT, false); break;
	case UP:
		this->motion.speedUp(UP, false); break;
	case DOWN:
		this->motion.speedUp(DOWN, false); break;
	}
	isGraphicInited = false;
}

// przypisanie bez gameEvents, wszystkie elelementy nie maja domyslnie gameEvents
Bullet::Bullet(EngineBase* engine, Json::Value& objectSrc)
	:
	DynamicObject(engine, objectSrc),
	bulletShape(engine, space)
{
	isGraphicInited = false;
}

Bullet::~Bullet()
{
}

void Bullet::serialize(Json::Value& objectDest)
{
	DynamicObject::serialize(objectDest);
}

int Bullet::getZIndex()
{
	return 0;
}

void Bullet::rotate(Direction direction)
{
	switch (direction){
		case LEFT: setRotation(180); break;
		case RIGHT:setRotation(0); break;
		case UP: setRotation(270); break;
		case DOWN: setRotation(90); break;
	}
}

GameRect* Bullet::getGameRect()
{
	return &bulletShape;
}

void Bullet::update()
{
	DynamicObject::update(); // faktyczna realizacja ruchu
}
void Bullet::draw()
{
	initGraphic();
	bulletShape.draw();
}

void Bullet::initGraphic()
{
	if (isGraphicInited == false)
	{
		isGraphicInited = true;

		bulletShape.setTexture("images/bullet.png", Space(0, 0, 15, 15));
		bulletShape.hideOutline();
		bulletShape.hideRectColor();
	}
}

ObjectType Bullet::getObjectType()
{
	return BULLET;
}

void Bullet::outOfMapEvent()
{
	engine->getObjectManager()->deleteObject(this);
}

CanMove Bullet::isMovable()
{
	return YES_CAN;
}

int Bullet::getLives()
{
	return 0;
}

CanMove Bullet::collision(GameObject* withObject)
{
	this->engine->getObjectManager()->deleteObject(this);
	this->setTransparet();
	return NO_CANT;
}
