#pragma once
#ifndef _POINT_HPP_
#define _POINT_HPP_

#include <SFML/System/Vector2.hpp>

using sf::Vector2f;

struct Point : Vector2f
{
	Point(float x, float y)
		: Vector2f(x, y)
	{
	}

	Point(int x, int y)
		: Vector2f(static_cast<float>(x), static_cast<float>(y))
	{
	}
};

#endif