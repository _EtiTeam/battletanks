#include "GameObject.hpp"
#include "JsonIndexes.hpp"
#include "EngineBase.hpp"

GameObject::GameObject(size_t id, EngineBase* engine, Space& space)
	:
	space(space)
{
	this->id = id;
	this->engine = engine;
	this->transparentFlag = false;

	if (engine != nullptr)	// warunek niespelniony tylko dla specyficznych obiektow jak EmptyObject
		lifeStartTime = engine->getLifeTime();
}

GameObject::GameObject(EngineBase* engine, Json::Value& objectSrc)
	:
	space(objectSrc[SPACE_])
{
	id = objectSrc[ID].asLargestUInt();
	transparentFlag = objectSrc[TRANSPARENT_FLAG].asBool();
	this->engine = engine;

	lifeStartTime = engine->getLifeTime() - sf::microseconds(objectSrc[OBJECT_LIFE_TIME].asLargestInt());
}

void GameObject::serialize(Json::Value& objectDest)
{	
	space.serialize(objectDest[SPACE_]);
	objectDest[ID] = id;
	objectDest[OBJECT_TYPE] = getObjectType();
	objectDest[OBJECT_LIFE_TIME] = getLifeTime().asMicroseconds();
	objectDest[TRANSPARENT_FLAG] = transparentFlag;
}

void GameObject::merge(GameObject* gameObject)
{
	if (id != gameObject->id)
		throw "podano obiekty o roznych id";

	merge(gameObject, true);
}

void GameObject::merge(GameObject* gameObject, bool isUnconditionalChangeSpace)
{
	//Time deltaTime = (getLifeTime() - gameObject->getLifeTime());
	//if (deltaTime >= sf::microseconds(0))	// jesli otrzymany obiekt jest starszy
	//	gameObject->update(deltaTime);		// synchronizacja TODO synchronizacja nie wuwzglednia gameEvents - nowe obiekty go nie posiadaja

	// merge dla nie dynamicznych obiektow
	if (isUnconditionalChangeSpace){
		space.setPosition(gameObject->space);
		space.setRotation(gameObject->space.getRotation());
	}
}

CanMove GameObject::collision(GameObject* withObject)
{
	return NO_CANT;
}

bool GameObject::isTransparent()
{
	return transparentFlag;
}

Space& GameObject::getSpace()
{
	return space;
}

Time GameObject::getLifeStartTime()
{
	return lifeStartTime;
}

Time GameObject::getLifeTime()
{
	return engine->getLifeTime() + getLifeStartTime();
}

EngineBase* GameObject::getEngine()
{
	return engine;
}

size_t GameObject::getId() const
{
	return id;
}

size_t GameObject::hash() const
{
	return id;
}

bool GameObject::equal_to(const GameObject& p) const
{
	return hash() == p.hash();
}

void GameObject::setTransparet()
{
	transparentFlag = true;
}

void GameObject::setId(size_t id)
{
	this->id = id;
}
