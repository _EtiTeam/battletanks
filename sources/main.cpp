#include "EngineClient.hpp"
#include "EngineServer.hpp"
#include <iostream>

int main()
{

	//EngineBase* game = new EngineClient();
//	EngineBase* game = new EngineServer();

	cout << "1 - serwer; 2 - klient (gra)" << endl;
	int i;
	cout << "wpisz ";
	cin >> i;

	EngineBase* game;
	if (i != 1)
		game = new EngineClient();
	else
		game = new EngineServer();


	while (game->isOpen())
	{
		game->update();
		game->draw();
	}

	delete game;
	return 0;
}
