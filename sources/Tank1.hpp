#pragma once
#ifndef _TANK1_HPP_
#define _TANK1_HPP_

#include <SFML/Graphics.hpp>
#include "Tank.hpp"

using sf::Texture;
using sf::Sprite;

// postac gracza
class Tank1
	: public Tank
{
public:
	Tank1(size_t id, EngineBase* engine, Point& point, GameEvents* gameEvents = nullptr, Color tankColor = Color::White);
	Tank1(EngineBase* engine, Json::Value& objectSrc);

	void serialize(Json::Value& objectDest) override final;
	void merge(GameObject* gameObject) override final;

	void initGraphic();
	void destroy();
	bool isDestroyed() override final;
	void draw() override;
	
	int getLives() override final;
	CanMove isMovable() override final;
	ObjectType getObjectType() override;
	CanMove collision(GameObject* withObject) override final;
private:
//	string texturePath; // zmiena niezbedna tylko do serializacji
//	Space textureSpace; // zmiena niezbedna tylko do serializacji
	Color tankColor;	// zmiena niezbedna tylko do serializacji
	bool destroyed;
	bool isGraphicInited;
};

#endif