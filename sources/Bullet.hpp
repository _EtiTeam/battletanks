#pragma once
#ifndef _BULLET_HPP_
#define _BULLET_HPP_

#include "DynamicObject.hpp"
#include "GameRect.hpp"
#include "Point.hpp"

// postac gracza
class Bullet : public DynamicObject
{
public:
	Bullet(size_t id, EngineBase* engine, Point& p, Direction direction);
	Bullet(EngineBase* engine, Json::Value& objectSrc);
	virtual ~Bullet();

	void serialize(Json::Value& objectDest) override;

	void update() override;
	void update(GameEvents* gameEvents) override final {}
	void draw() override;
	void initGraphic();

	int getLives() override;
	int getZIndex() override;
	ObjectType getObjectType() override;
	
	void outOfMapEvent() override final;
	CanMove isMovable() override;
	CanMove collision(GameObject* withObject) override final;
	
	// metody zbedne gdyz GameRect jest polaczony z shape wbudowany w klase
	//virtual Space& getSpace() override;
	//virtual void setSpace(Space& space) override;

	void rotate(Direction direction);
	GameRect* getGameRect() override;

	void unbindGameEvents() override {}		// brak zalozen odnosnie sterowania
protected:
	GameRect bulletShape;
	//int lives;
	const static int Bullet_WIDTH = 10;
	const static int Bullet_HEIGHT = 10;
private:
	bool isGraphicInited;
};

#endif