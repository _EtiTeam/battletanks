#pragma once
#ifndef _OTHER_FUNCTIONS_HPP_
#define _OTHER_FUNCTIONS_HPP_
#include <string>
#include <sstream>
#include <SFML/Graphics/RenderWindow.hpp>

using namespace std;

template<typename T>
string toString(T number)
{
	static stringstream ss;
	ss.clear();
	ss << number;
	string str;
	ss >> str;
	return str;
}

inline string toString(size_t number)
{
	string str;
	if (number == 0)
		str = "0";
	while (number > 0)
	{
		str.push_back(number % 10 + '0');
		number /= 10;
	}
	return string(str.rbegin(), str.rend());
}

bool isFocus(sf::RenderWindow* app);

unsigned numberOfDigits(unsigned i);

#endif
