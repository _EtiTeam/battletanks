#include "Motion.hpp"
#include "DynamicObject.hpp"
#include "EngineClient.hpp"
#include "json/value.h"

Motion::Motion(DynamicObject& dynamicObject, const SpeedProperty& speedProperty)
	:
	speedProperty(speedProperty),
	dynamicObject(dynamicObject)
{
	xSpeed = 0;
	ySpeed = 0;
	xMovement = 0;
	yMovement = 0;
}

Motion::Motion(DynamicObject& dynamicObject, Json::Value& objectSrc)
	:
	speedProperty({ 
		DEFAULT_SPEED_DOWN_IDLE, DEFAULT_SPEED_DOWN, DEFAULT_SPEED_UP, DEFAULT_MAX_SPEED
		//objectSrc["speedDownIdle"].asFloat(),
		//objectSrc["speedDown"].asFloat(),
		//objectSrc["speedUp"].asFloat(),
		//objectSrc["maxSpeed"].asFloat()
	}),
	dynamicObject(dynamicObject)
{
}

void Motion::serialize(Json::Value& objectDest)
{
	objectDest["xSpeed"] = xSpeed;
	objectDest["ySpeed"] = ySpeed;
	objectDest["xMovement"] = xMovement;
	objectDest["yMovement"] = yMovement;

	//objectDest["speedDownIdle"] = speedProperty.speedDownIdle;
	//objectDest["speedDown"] = speedProperty.speedDown;
	//objectDest["speedUp"] = speedProperty.speedUp;
	//objectDest["maxSpeed"] = speedProperty.maxSpeed;
}

void Motion::update()
{
	auto time = getDeltaTimeSeconds();
	update(time);
}

void Motion::update(float time)
{
	xMovement += xSpeed * time;
	yMovement += ySpeed * time;

	moveX();
	moveY();

	idleXSpeedDown();
	idleYSpeedDown();
}

void Motion::speedUp(Direction direction, bool isTimeBased)
{
	if (dynamicObject.isMovable() == TRUE){
		switch (direction){
			case LEFT: leftSpeedUp(isTimeBased); break;
			case RIGHT: rightSpeedUp(isTimeBased); break;
			case UP: upSpeedUp(isTimeBased); break;
			case DOWN: downSpeedUp(isTimeBased); break;
		}
	}
}

void Motion::merge(const Motion& motion)
{
	xSpeed = motion.xSpeed;
	ySpeed = motion.ySpeed;
	xMovement = motion.xMovement;
	yMovement = motion.yMovement;
	speedProperty = motion.speedProperty;
}

float Motion::getXSpeed()
{
	return ySpeed;
}

float Motion::getYSpeed()
{
	return xSpeed;
}

void Motion::leftSpeedUp(bool isTimeBased)
{
	float time = 1;
	if (isTimeBased)
		time = getDeltaTimeSeconds();

	if (xSpeed <= 0){
		xSpeed -= speedProperty.speedUp * time;
		// ograniczenie max predkosc
		if (xSpeed < -speedProperty.maxSpeed)
			xSpeed = -speedProperty.maxSpeed;
	}
	else{
		// hamowanie
		xSpeed -= speedProperty.speedDown * time;
		if (xSpeed < 0)
			xSpeed = 0;
	}
}

void Motion::rightSpeedUp(bool isTimeBased)
{
	float time = 1;
	if (isTimeBased)
		time = getDeltaTimeSeconds();
	if (xSpeed >= 0){
		xSpeed += speedProperty.speedUp * time;
		// ograniczenie max predkosc
		if (xSpeed > speedProperty.maxSpeed)
			xSpeed = speedProperty.maxSpeed;
	}
	else{
		// hamowanie
		xSpeed += speedProperty.speedDown * time;
		if (xSpeed > 0)
			xSpeed = 0;
	}
}

void Motion::upSpeedUp(bool isTimeBased)
{
	float time = 1;
	if (isTimeBased)
		time = getDeltaTimeSeconds();
	if (ySpeed <= 0){
		ySpeed -= speedProperty.speedUp * time;
		// ograniczenie max predkosc
		if (ySpeed < -speedProperty.maxSpeed)
			ySpeed = -speedProperty.maxSpeed;
	}
	else{
		// hamowanie
		ySpeed -= speedProperty.speedDown * time;
		if (ySpeed < 0)
			ySpeed = 0;
	}
}

void Motion::downSpeedUp(bool isTimeBased)
{
	float time = 1;
	if (isTimeBased)
		time = getDeltaTimeSeconds();
	if (ySpeed >= 0){
		ySpeed += speedProperty.speedUp * time;
		// ograniczenie max predkosc
		if (ySpeed > speedProperty.maxSpeed)
			ySpeed = speedProperty.maxSpeed;
	}
	else{
		// hamowanie
		ySpeed += speedProperty.speedDown * time;
		if (ySpeed > 0)
			ySpeed = 0;
	}
}

void Motion::idleXSpeedDown()
{
	if (xSpeed != 0){
		// warunek graniczy
		// spelniony tylko gdy zmniejszenie obecej predkosci powoduje
		// zwiekszenie predkosci w druga strone
		auto idleSpeedDown = speedProperty.speedDownIdle * getDeltaTimeSeconds();
		if ((fabs(xSpeed) - idleSpeedDown) < 0)
			xSpeed = 0;
		else if (xSpeed < 0)
			xSpeed += idleSpeedDown;
		else
			xSpeed -= idleSpeedDown;
	}
}

void Motion::idleYSpeedDown()
{
	if (ySpeed != 0){
		// warunek graniczy
		// spelniony tylko gdy zmniejszenie obecej predkosci powoduje
		// zwiekszenie predkosci w druga strone
		auto idleSpeedDown = speedProperty.speedDownIdle * getDeltaTimeSeconds();
		if ((fabs(ySpeed) - idleSpeedDown) < 0)
			ySpeed = 0;
		else if (ySpeed < 0)
			ySpeed += idleSpeedDown;
		else
			ySpeed -= idleSpeedDown;
	}
}

bool Motion::moveX(float xMovement)
{
	// wykorzystanie juz zaimplementowanych funkcji wykorzystujace 
	// pola zawarte w strukturze
	auto xSpeed = this->xSpeed;
	swap(xMovement, this->xMovement);

	bool result = moveX();
	
	// przywrocenie kopii + resztki
	this->xMovement += xMovement;
	this->xSpeed = xSpeed;
	return result;
}

bool Motion::moveY(float yMovement)
{
	// wykorzystanie juz zaimplementowanych funkcji wykorzystujace 
	// pola zawarte w strukturze
	auto ySpeed = this->ySpeed;
	swap(yMovement, this->yMovement);
	
	bool result = moveY();

	// przywrocenie kopii + resztki
	this->yMovement += yMovement;
	this->ySpeed = ySpeed;
	return result;
}

bool Motion::moveX()
{
	bool result;
	while (xMovement >= 1.0f)
	{
		result = dynamicObject._move(1, 0);
		--xMovement;
		if (result == false) // przeszkoda
		{
			xMovement = 0;
			xSpeed = 0;
			return false;
		}
	}

	while (xMovement <= -1.0f)
	{
		result = dynamicObject._move(-1, 0);
		++xMovement;
		if (result == false) // przeszkoda
		{
			xMovement = 0;
			xSpeed = 0;
			return false;
		}
	}

	return true;
}

bool Motion::moveY()
{
	bool result;
	while (yMovement >= 1.0f)
	{
		result = dynamicObject._move(0, 1);
		--yMovement;
		if (result == false) // przeszkoda
		{
			yMovement = 0;
			ySpeed = 0;
			return false;
		}
	}

	while (yMovement <= -1.0f)
	{
		result = dynamicObject._move(0, -1);
		++yMovement;
		if (result == false) // przeszkoda
		{
			yMovement = 0;
			ySpeed = 0;
			return false;
		}
	}
	return true;
}

float Motion::getDeltaTimeSeconds()
{
	return dynamicObject.getEngine()->getDeltaTime().asSeconds();
}
void Motion::setSpeedProperty(const SpeedProperty& spdp)
{
	this->speedProperty = spdp;
}
