#include "otherFunctions.hpp"
#include <windows.h>

bool isFocus(sf::RenderWindow* app)
{
	if (app == nullptr)
		return false;

	HWND handle = app->getSystemHandle();
	bool one = handle == GetFocus();
	bool two = handle == GetForegroundWindow();

	//if (one != two) //strange 'half-focus': window is in front but you can't click anything - so we fix it
	//{
	//	SetFocus(handle);
	//	SetForegroundWindow(handle);
	//}

	return one && two;
}

unsigned numberOfDigits(unsigned i)
{
	return i > 0 ? static_cast<int>(log10(static_cast<double>(i))) + 1 : 1;
}
