#pragma once
#ifndef _GAME_EVENTS_HPP_
#define _GAME_EVENTS_HPP_

#include "json/json.h"
#include "JsonIndexes.hpp"
#include <SFML/Graphics.hpp>

class GameObject;
class EngineBase;

enum Direction
{
	NONE = 0,
	LEFT, RIGHT, UP, DOWN
};

enum Control
{
	NONE_CTRL = 0,
	ENTER,
	ESCAPE,
	SPACE
};

class GameEvents abstract
{
public:
	GameEvents(EngineBase* engine, bool readAllKey);
	GameEvents(Json::Value& json);
	virtual ~GameEvents() {}
	virtual void update(); // odbieranie wszystkich zdarzen
	virtual void update(Json::Value& value);

	virtual void serialize(Json::Value& json);

	bool isReadAllKey();
	void offReadAllKey();
	void onReadAllKey();
	Control getControl();
	Direction getBlockedDirection();
	virtual Direction getDirection() = 0;
	virtual GameObject* getBindedObject() =0;
	virtual void setBindedObject(GameObject*) = 0;
	virtual bool makeShoot() = 0;

protected:
	EngineBase* engine;
	bool closeSignal;

private:
	bool readAllKey;	// flaga - ture = po update wywolana jest update z GameEvent = odczyt klawiszy sterujacych

	Control control;
	Direction blockingDirection;	// zmienna ktora jest sprawdzana wraz z usuwaniem z event
									// zachowuje opoznienia, jak przy pisaniu
};

#endif