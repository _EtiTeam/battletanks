#include "GameObjectSet.hpp"
#include "otherFunctions.hpp"
#include "EngineBase.hpp"
#include <windows.h>

GameObjectSet_notAutoDeletedAndMerged::GameObjectSet_notAutoDeletedAndMerged()
{
	InitializeCriticalSection(&section);
}

GameObjectSet_notAutoDeletedAndMerged::~GameObjectSet_notAutoDeletedAndMerged()
{
	clear();
	DeleteCriticalSection(&section);
}

void GameObjectSet_notAutoDeletedAndMerged::clear()
{
	EnterCriticalSection(&section);
	unordered_set::clear();
	LeaveCriticalSection(&section);
}

void GameObjectSet_notAutoDeletedAndMerged::erase(GameObject* gameObject)
{
	EnterCriticalSection(&section);
	unordered_set::erase(gameObject);
	LeaveCriticalSection(&section);
}

void GameObjectSet_notAutoDeletedAndMerged::insert(GameObject* gameObject)
{
	EnterCriticalSection(&section);
	unordered_set::insert(gameObject);
	LeaveCriticalSection(&section);
}

void GameObjectSet_notAutoDeletedAndMerged::erase(iterator& it)
{
	EnterCriticalSection(&section);
	unordered_set::erase(it);
	LeaveCriticalSection(&section);
}

void GameObjectSet_notAutoDeletedAndMerged::serialize(Json::Value& objectDest)
{
	Json::Value newObject;
	EnterCriticalSection(&section);

	for (auto it = begin(); it != end(); ++it)
	{
		newObject.clear();
		auto id = toString((*it)->getId());
		(*it)->serialize(newObject);
		objectDest[id] = newObject;
	}

	LeaveCriticalSection(&section);
}

unordered_set<GameObject*>::iterator GameObjectSet_notAutoDeletedAndMerged::find(GameObject* gameObject)
{
	EnterCriticalSection(&section);
	auto it = unordered_set::find(gameObject);
	LeaveCriticalSection(&section);
	return it;
}

unordered_set<GameObject*>::iterator GameObjectSet_notAutoDeletedAndMerged::begin()
{
	EnterCriticalSection(&section);
	auto it = unordered_set::begin();
	LeaveCriticalSection(&section);
	return it;
}

unordered_set<GameObject*>::iterator GameObjectSet_notAutoDeletedAndMerged::end()
{
	EnterCriticalSection(&section);
	auto it = unordered_set::end();
	LeaveCriticalSection(&section);
	return it;
}

//////////// GameObjectSet //////////////////////////////////////////////

GameObjectSet::GameObjectSet(EngineBase* engine)
	: gameObjectFactory(engine)
{
}

GameObjectSet::~GameObjectSet()
{
	EnterCriticalSection(&section);
	clear();
	LeaveCriticalSection(&section);
}

void GameObjectSet::clear()
{
	EnterCriticalSection(&section);
	for (auto it = begin(); it != end(); ++it)
		delete *it;
	unordered_set::clear();
	LeaveCriticalSection(&section);
}

void GameObjectSet::merge(Json::Value& objectSrc)
{
	EnterCriticalSection(&section);
	for (auto it = objectSrc.begin(); it != objectSrc.end(); ++it){
		auto& jsonObject = *it;
		auto objectTypeInt = jsonObject[OBJECT_TYPE].asInt();
		ObjectType objectType = static_cast<ObjectType>(objectTypeInt);
		insert(gameObjectFactory.get(objectType, jsonObject));
	}
	LeaveCriticalSection(&section);
}

void GameObjectSet::insert(GameObject* gameObject)
{
	EnterCriticalSection(&section);

	auto it = find(gameObject);
	if (it != end())
	{
		if (*it != gameObject){			// obiekty o tych samych adresach nie powinny byc aktualizowane
			(*it)->merge(gameObject);	// merge - zmiana pozycji po pikselu
			delete gameObject;			// po scaleniu obiekt nie jest juz potrzebny
		}
	}
	else
		unordered_set::insert(gameObject);

	LeaveCriticalSection(&section);
}
