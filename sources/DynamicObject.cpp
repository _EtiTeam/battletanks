#include "DynamicObject.hpp"
#include "EngineClient.hpp"

DynamicObject::DynamicObject(size_t id, EngineBase* engine, Space& space)
	:
	GameObject(id, engine, space),
	motion(*this)
{
}

DynamicObject::DynamicObject(EngineBase* engine, Json::Value& objectSrc)
	:
	GameObject(engine, objectSrc),
	motion(*this, objectSrc[MOTION])
{
}

DynamicObject::~DynamicObject()
{
}

void DynamicObject::serialize(Json::Value& objectDest)
{
	motion.serialize(objectDest[MOTION]);
	GameObject::serialize(objectDest);
}

bool DynamicObject::_move(float x, float y)
{
	static Space space(0,0,0,0);
	space = getSpace(); // kopia
	space.move(x,y);
	return _setPosition(space);
}

bool DynamicObject::setRotation(float angle)
{
	if (isMovable() == TRUE)
	{
		Space space = getSpace(); // kopia
		space.setRotation(angle);
		if (isCollision(space)) // jesli nie ma kolizji
		{
			getGameRect()->setRotation(angle); // niezbedne do poprawnego funkcjonowania - rotacja np. tekstury
			this->space.setRotation(space.getRotation());
			return true;
		}
	}
	return false;
}

bool DynamicObject::_setPosition(Space& newSpace)
{
	auto objectManager = engine->getObjectManager();
	
	if (objectManager->isAvaible(newSpace) == false){
		outOfMapEvent();
		return false;
	}

	// sprawdzenie kolizji
	auto gameObject = objectManager->isCollision(*this, newSpace);

	// obsluga kolizji jesli jest tam obiekt
	CanMove canMove = YES_CAN;
	if (gameObject != nullptr)
	{
		canMove = collision(gameObject); // wykonanie kolizji wraz z wczytaniem mozliwosci
		gameObject->collision(this);
	}

	if (canMove == NO_CANT)
		return false;

	// pod powyzszymi warunkami zmieniona zostaje space
	setSpace(newSpace);
	return true;
}

void DynamicObject::setSpace(Space& space)
{
	this->space.setPosition(space);
}

void DynamicObject::update()
{
	motion.update();
}

void DynamicObject::update(Time& deltaTime)
{
	if (deltaTime < sf::microseconds(0))
		throw "nie mozna zauktualizwac do starszej wersji";
	motion.update(deltaTime.asSeconds());
}

void DynamicObject::merge(GameObject* gameObject)
{
	// wywolanie dla GameObject
	GameObject::merge(gameObject, false);

	auto object = reinterpret_cast<DynamicObject*>(gameObject);
	setRotation(object->space.getRotation());	// reczna aktualizacja
	setPosition(object->space);					// zmiana tylko pozycji
	motion.merge(object->motion);
}

bool DynamicObject::isCollision(Space& newSpace)
{
	// czesc metody _setPosition
	auto objectManager = engine->getObjectManager();

	if (objectManager->isAvaible(newSpace) == false)
		return false;

	// sprawdzenie kolizji
	auto gameObject = objectManager->isCollision(*this, newSpace);
	if (gameObject != nullptr)
		return false;
	
	// tylko jesli wszystko powyzsze spelnione
	return true;
}



bool DynamicObject::move(float x, float y)
{
	
	bool result1 = false;
	if (isMovable() == TRUE)
		result1 = motion.moveX(x);
	bool result2 = false;
	if (isMovable() == TRUE)
		result2= motion.moveY(y);
	return result1 && result2;
}

bool DynamicObject::setPosition(Space& newSpace)
{
	auto& space = getSpace();
	// warunek poprawnosci - nie jest spelniony ze wzgledu na zaokraglenia zmiennych
	//if ((newSpace.getWidth() != space.getWidth()) ||
	//	(newSpace.getHeight() != space.getHeight()))
	//	throw "podano obiekty o nie identycznych powierzchniach";
	
	// przesuniecie
	auto xMovement = newSpace.x1 - space.x1;
	auto yMovement = newSpace.y1 - space.y1;

	return move(xMovement, yMovement);
}


