#pragma once
#ifndef _TOOLBAR_HPP_
#define _TOOLBAR_HPP_
#include <SFML/Graphics.hpp>

class EngineBase;
using namespace sf;

class EngineClient;

class Toolbar{
public:	
	Toolbar(EngineClient* engine);
	void draw();
private:
	EngineClient* engine;
	Vector2u position;
	Vector2f size;
	Font font;

	// bar field
	RectangleShape barBackground;
	Texture barBackgroundTexture;
	String barPath;

	// hearth icons
	Text lifeLabel;
	Texture heartTexture;
	RectangleShape heartIcons[3];
	
	String heartPath;

	// connection info
	Text gameIdInfo;
	RectangleShape connectionStatus;

	RectangleShape progresBar;
	clock_t t1,t2;
	int it;
	float progressBarSizeX;
	float progressBarSizeY;
	float progressBarPosX;
	float tempProgressBarPosX;



};
#endif