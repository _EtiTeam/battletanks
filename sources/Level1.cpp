#include "Level1.hpp"

#include <SFML/System/String.hpp>
#include <algorithm>
#include "EngineClient.hpp"
#include "Wall.hpp"

using sf::String;

Level1::Level1(EngineBase* engineBase)
	:
	Level(engineBase),
	levelSpace(0.0f, 0.0f, engineBase->SCREEN_WIDTH, engineBase->SCREEN_HEIGHT)
{
	setBackground("images\\background.jpg");
	//drawLineVertical(100, 500, 300);
	//drawLineVertical(100, 500, 320);
	//drawLineVertical(100, 500, 500);
	//drawLineVertical(100, 500, 520);
	//drawLineVertical(0,100 , 400);
	//drawLineHorizontal(320, 500, 100);
	//drawLineHorizontal(320, 500, 500);
	//staticObjects.push_back(new Wall(engine, levelSpace));
}

Level1::~Level1()
{
}

Space& Level1::getLevelSpace()
{
	return levelSpace;
}

