#pragma once
#ifndef _GAME_OBJECT_SET_HPP_
#define _GAME_OBJECT_SET_HPP_

//#include "GameObject.hpp"
#include "GameObjectFactory.hpp"
#include <unordered_set>
#include <windows.h>

class EngineBase;
class GameObject;

using std::unordered_set;
using namespace std;

// obiekt + jego id
// klasa wykorzystywana przez set
// utworzona aby nie zmieniac struktury gameObject

namespace std
{
	template<>
	struct hash <GameObject*> {
		size_t operator()(GameObject* gameObject) const{
			return gameObject->hash();
		}
	};

	template<>
	struct equal_to<GameObject*>
	{
		bool operator()(GameObject* l, GameObject* p) const{
			return l->equal_to(*p);
		}
	};
}

class GameObjectSet_notAutoDeletedAndMerged : protected unordered_set<GameObject*>
{
public:
	GameObjectSet_notAutoDeletedAndMerged();
	virtual ~GameObjectSet_notAutoDeletedAndMerged();

	virtual void clear();
	virtual void insert(GameObject* gameObject);
	virtual void erase(GameObject* gameObject) final;
	virtual void erase(iterator& gameObjectIt) final;
	virtual void serialize(Json::Value& jsonDest) final;
	virtual iterator find(GameObject* gameObject) final;
	

	virtual iterator begin() final;
	virtual iterator end() final;
protected:
	CRITICAL_SECTION section;

};

class GameObjectSet : public GameObjectSet_notAutoDeletedAndMerged
{
public:
	GameObjectSet(EngineBase* engine);
	virtual ~GameObjectSet();

	void clear() override final;
	// nie usuwac obiektu na ktorym uzylo sie tej metody
	void insert(GameObject* gameObject) override final;
	virtual void merge(Json::Value& jsonObj) final;
private:
	GameObjectFactory gameObjectFactory;
};

#endif
