#include "StaticObject.hpp"

StaticObject::StaticObject(size_t id, EngineBase* engine, Space& space)
	: 
	GameObject(id, engine, space)
{
}

StaticObject::StaticObject(EngineBase* engine, Json::Value& objectSrc)
	: GameObject(engine, objectSrc)
{
}

StaticObject::~StaticObject()
{
}

void StaticObject::update()
{
}

CanMove StaticObject::collision(GameObject* withObject)
{
	return NO_CANT;
}
