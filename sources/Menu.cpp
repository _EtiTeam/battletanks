#include "EngineClient.hpp"
#include "Menu.hpp"
#include <windows.h>

enum TypeButton
{
	PLAY = 0,
	NETWORK = 1,
	EXIT = 2
};

void updateVal(string s, Text* IPValue, int* IPinputPosition)
{
	if (IPValue->getString().getSize() < 13){
		string ipVal = IPValue->getString();
		ipVal.append(s);
		IPValue->setString(ipVal);
	}
}

void Menu::KeyValue(Text* IPValue, int* IPinputPosition)
{
	RenderWindow* gameWindow = engine->getGameWindow();
	Event event;
	while (gameWindow->pollEvent(event))
	{
		switch (event.type)
		{
			case (Event::KeyPressed) :
			{
				switch (event.key.code)
				{
					case (Keyboard::Num1) :
						updateVal("1", IPValue, IPinputPosition);
						break;
					case (Keyboard::Num2) :
						updateVal("2", IPValue, IPinputPosition);
						break;
					case (Keyboard::Num3) :
						updateVal("3", IPValue, IPinputPosition);
						break;
					case (Keyboard::Num4) :
						updateVal("4", IPValue, IPinputPosition);
						break;
					case (Keyboard::Num5) :
						updateVal("5", IPValue, IPinputPosition);
						break;
					case (Keyboard::Num6) :
						updateVal("6", IPValue, IPinputPosition);
						break;
					case (Keyboard::Num7) :
						updateVal("7", IPValue, IPinputPosition);
						break;
					case (Keyboard::Num8) :
						updateVal("8", IPValue, IPinputPosition);
						break;
					case (Keyboard::Num9) :
						updateVal("9", IPValue, IPinputPosition);
						break;
					case (Keyboard::Num0) :
						updateVal("0", IPValue, IPinputPosition);
						break;
					case (Keyboard::Period) :
						updateVal(".", IPValue, IPinputPosition);
						break;
					case (Keyboard::BackSpace) :
						if (IPValue->getString().getSize() > 0)
						{
							string ipVal = IPValue->getString();
							ipVal.resize(ipVal.size() - 1);
							IPValue->setString(ipVal);
						}
						break;
					case (Keyboard::Return) :
						setActive(false);
						setSubmenu(false);
						engine->networkConnect(IPValue->getString());
						break;						 
				}
			}
		}
	}
}

Menu::Menu(EngineClient* engine)
{
	active = false;
	IPinputPosition = 0;
	//this->setVisible(true);
	currentButton = 0;
	buttons.push_back(new Button(engine, PLAY));
	buttons.push_back(new Button(engine, NETWORK));
	buttons.push_back(new Button(engine, EXIT));
	this->engine = engine;
	this->position = Vector2u(engine->SCREEN_WIDTH / 4, engine->SCREEN_HEIGHT / 5);
	this->size = Vector2f(400.0, 500.0);
	menuBackgroundPath = "images/toolbar.png";
	if (!menuBackgroundTexture.loadFromFile(menuBackgroundPath))
		printf("error obrazka");

	menuBackground.setTexture(&menuBackgroundTexture);
	menuBackground.setSize(size);
	menuBackground.setPosition(static_cast<float>(position.x) + 50, static_cast<float>(position.y));

	if (!this->menuFont.loadFromFile("fonts/comic.ttf")) throw "Brak czcionki";

	menuLabel.setString("Menu");
	menuLabel.setColor(Color::White);
	menuLabel.setPosition(static_cast<float>(position.x) + size.x / 2.0f + 5.0f, 0.01f*size.y + position.y + 30.0f);
	menuLabel.setCharacterSize(32);
	menuLabel.setFont(menuFont);
	//playButton2.setActive(true);
	IPValue.setString("127.0.0.1");
	//IPValue.setString("192.168.0.12");
	IPValue.setColor(Color::Cyan);
	IPValue.setPosition(static_cast<float>(position.x) + size.x / 2.0f - 75.0f, 0.01f*size.y + position.y + 150.0f);
	IPValue.setCharacterSize(64);
	IPValue.setFont(menuFont);
}

void Menu::draw()
{
	if (isActive())
	{
		auto window = engine->getGameWindow();
		if (!isSubmenu()){
			window->draw(menuBackground);
			window->draw(menuLabel);


			for (int it = 0; it < buttons.size(); ++it)
			{
				if (it == currentButton)
					buttons.at(it)->setActive(true);
				else
				{
					buttons.at(it)->setActive(false);
				}
				buttons.at(it)->draw();
			}
		}
		else
		{	//* wyswietlanie podmenu

			KeyValue(&IPValue, &IPinputPosition);
			window->draw(menuBackground);
			window->draw(menuLabel);
			window->draw(IPValue);

		}
	}


}

bool Menu::isActive()
{
	return active;
}

void Menu::setActive(bool active)
{
	if (active){
		engine->getObjectManager()->gameEventsOff();
		engine->setGameRunning(false);
	}
	else
	{
		engine->getObjectManager()->gameEventsOn();
		engine->setGameRunning(true);
	}
	this->active = active;
}

bool Menu::isSubmenu()
{
	return subMenuFlag;
}

void Menu::changeActive()
{
	setActive(!active); // na przeciwny
}

void Menu::previousButton()
{
	if (currentButton > 0)
		this->currentButton--;
	//Sleep(200);
}

void Menu::nextButton()
{
	if (currentButton < buttons.size() - 1)
		this->currentButton++;
	//Sleep(200);
}

void Menu::escape()
{
	buttons[currentButton]->escape();
}

void Menu::update()
{
	// uruchomienie/wylacznie menu
	// inna sytuacja jest gdy jest wlaczone submenu
	auto control = engine->getMainGameUserEvents()->getControl();
	if (isSubmenu() == false && control == ESCAPE)
		changeActive();

	if (isActive())
	{
		auto direction = engine->getMainGameUserEvents()->getBlockedDirection();
		//* Aktywowane menu...
		//* Sterowanie przyciskami w menu,
		if (direction == UP) previousButton();
		else if (direction == DOWN) nextButton();
		else if (control == ENTER) enter();
		else if (control == ESCAPE) escape();
	}
}

void Menu::setSubmenu(bool active)
{
	if (active)
		engine->getMainGameUserEvents()->offReadAllKey();
	else
		engine->getMainGameUserEvents()->onReadAllKey();

	subMenuFlag = active;
}

void Menu::enter()
{
	buttons[currentButton]->enter();
}

void Menu::subMenu()
{
	menuLabel.setString("Type IP");
	setSubmenu(true);
}

void Menu::mainMenu()
{
	currentButton = 0;
	setSubmenu(false);
	menuLabel.setString("Menu");
}