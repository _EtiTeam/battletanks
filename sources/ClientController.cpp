#include "ClientController.hpp"
#include "EngineClient.hpp"

#include <iostream>

ClientController::ClientController(EngineBase* engine, const string& serverAddr, u_short serverPort)
	:
	NetworkController(engine),
	toRecv(TO_RECV_BUFFOR_SIZE),
	toSend(TO_SEND_BUFFOR_SIZE)
{
	started = false;
	DWORD thredId;
	// przytrzymywanie danych dla watkow
	clientConnectCfg[0] = { serverAddr, serverPort, engine, serviceReceivingSocket, this, RECEIVE };
	clientConnectCfg[1] = { serverAddr, serverPort, engine, serviceSendingSocket, this, SENDING };

	auto handle = CreateThread(nullptr, 1 << 24, startClient, static_cast<void*>(&clientConnectCfg[0]), 0, &thredId);
	addThreadHandle(handle);
	// DRUGI Z WATKOW ROZPOCZYNA PRACE PO WYKONANIU METODY START
}

ConcurrentQueue<Json::Value*>& ClientController::getToSendQueue()
{
	return toSend;
}

ConcurrentQueue<Json::Value*>& ClientController::getToReceiveQueue()
{
	return toRecv;
}


void ClientController::start()
{
	DWORD thredId;
	auto handle = CreateThread(nullptr, 1 << 24, startClient, static_cast<void*>(&clientConnectCfg[1]), 0, &thredId);
	started = true;
	addThreadHandle(handle);
}

void ClientController::update()
{
	if (isStarted()){
		// wysy�anie
		auto mainEvents = engine->getMainGameUserEvents();
		if (mainEvents->getBindedObject() != nullptr){
			Json::Value* jsonToSend = new Json::Value;
			mainEvents->serialize(*jsonToSend);
			getToSendQueue().push(&jsonToSend);
		}

		// odbieranie
		Json::Value* jsonToReceive = nullptr;
		if (getToReceiveQueue().tryPop(&jsonToReceive))
		{
			engine->merge(*jsonToReceive);	// scalenie
			//cout << "czolg" << endl << *jsonToReceive;
			//this->engine->getObjectManager()->serializeChanges(json);
		}
		delete jsonToReceive;
	}
}

bool ClientController::isStarted()
{
	return started;
}

DWORD ClientController::serviceSendingSocket(void* serviceCfgVoid)
{
	ServiceCfg* serviceCfg = static_cast<ServiceCfg*>(serviceCfgVoid);
	SOCKET& socket = serviceCfg->socket;
	EngineBase* engine = serviceCfg->engine;

	Json::Value json;
	Json::Value* jsonSending = nullptr;
	auto engineSection = engine->getCriticalSection();

	cout << "serviceSendingSocket start" << endl;
	// ############ WYSLANIE WLASNYCH OBIEKTOW  #########################
	EnterCriticalSection(engineSection);
	serviceCfg->engine->getObjectManager()->serializeChanges(json);
	LeaveCriticalSection(engineSection);
	send(socket, json);

	// wysylanie gameEventsow
	while (true){
		//EnterCriticalSection(engineSection);
		//serviceCfg->engine->getObjectManager()->serializeChanges(json);		// tylko zmiany
		// przywidziane tylko dla jednego gracza sterowanie
		
			//TODO//engine->getGameEventsToSend()->pop(json);
		//LeaveCriticalSection(engineSection);
		// TODO nie wysylac jesli nie ma zmian
		jsonSending = serviceCfg->networkController->getToSendQueue().pop();
		send(socket, *jsonSending);
		//Sleep(10);
		delete jsonSending;
	}
	return 0;
}

DWORD ClientController::serviceReceivingSocket(void* serviceCfgVoid)
{
	ServiceCfg* serviceCfg = static_cast<ServiceCfg*>(serviceCfgVoid);
	SOCKET& socket = serviceCfg->socket;
	EngineClient* engine = dynamic_cast<EngineClient*>(serviceCfg->engine);

	Json::Value json;
	Json::Value* jsonReceived = nullptr;
	char buff[BUFF_SIZE];
	string strBuff;
	strBuff.reserve(BUFF_SIZE);
	auto engineSection = engine->getCriticalSection();

	cout << "serviceReceivigSocket start" << endl;

	// ############ INICJALIZACJA CLIENTA ##############################
	recv(socket, json, buff,strBuff);
	cout << "serviceReceivigSocket odebrano dane od serwera" << endl;
	engine->networkConnectionEstablish(json);
	cout << "serviceReceivigSocket zainicjalizowano" << endl;

	// ############ ODBIOR DANYCH ######################################
	while (true)
	{
		jsonReceived = new Json::Value();
		recv(socket, *jsonReceived, buff, strBuff);

		cout << "SIZE " << serviceCfg->networkController->getToReceiveQueue().size() << endl;
		//cout << *jsonReceived << endl;

		serviceCfg->networkController->getToReceiveQueue().blockingPush(&jsonReceived);
		delete jsonReceived; // jesli nie dodano 
	}
	return 0;
}
