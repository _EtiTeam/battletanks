#pragma once
#ifndef _GAME_USER_EVENTS_HPP_
#define _GAME_USER_EVENTS_HPP_

#include "GameEvents.hpp"


class GameUserEvents : public GameEvents
{
public:
	GameUserEvents(EngineBase* engine, bool readAllKey = false);
	GameUserEvents(Json::Value& json);

	virtual ~GameUserEvents();

	void serialize(Json::Value& json) override final;

	void update() override final; // odbieranie wszystkich zdarzen
	void update(Json::Value& json) override final;

	Direction getDirection() override final;
	GameObject* getBindedObject() override final;
	void setBindedObject(GameObject* obj) override final;
	bool makeShoot() override final;
	
private:
	Direction direction;
	GameObject* bindedObject;
	bool shoot;
};

#endif