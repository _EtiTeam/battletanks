#pragma once
#ifndef _MENU_HPP_
#define _MENU_HPP_

#include "Button.hpp"
#include <SFML/Graphics.hpp>

class Menu
{
	friend class Button;
public:
	void KeyValue(Text* IPValue, int* IPinputPosition);
	Menu(EngineClient* engine);
	void draw();
	bool isActive();
	void setActive(bool active);
	void changeActive();
	bool isSubmenu();
	
	void previousButton();
	void nextButton();
	void update();
private:
	void setSubmenu(bool active);

	void enter();
	void subMenu();
	void mainMenu();
	void escape();

	EngineClient* engine;

	Vector2u position;
	Vector2f size;
	
	// menu field
	RectangleShape menuBackground;
	Texture menuBackgroundTexture;
	String menuBackgroundPath;
	bool active;
	std::vector<Button*> buttons;
	

	

	// menu Text
	Text menuLabel;
	Text IPValue;
	Font menuFont;
	int currentButton;
	bool subMenuFlag;
	int IPinputPosition;
};
#endif
