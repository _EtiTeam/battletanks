#include "EngineClient.hpp"
#include "GameUserEvents.hpp"
#include "GameUserEvents2.hpp"
#include "Level1.hpp"
#include "Tank1.hpp"
#include "Bullet.hpp"
#include "ServerController.hpp"
#include "ClientController.hpp"
#include "Toolbar.hpp"

#include <iostream>


#define BORDO 105, 15, 40

EngineClient::EngineClient()
	:
	EngineBase(),
	gameWindow(VideoMode(static_cast<unsigned>(SCREEN_WIDTH), static_cast<unsigned>(SCREEN_HEIGHT)), "Battletank"),
	gameMenu(this),
	
	gameCamera(this, nullptr)
{
	GameUserEvents* arrowGameUserEvents = new GameUserEvents(this, true);
	GameUserEvents2* WASDGameUserEvents = new GameUserEvents2(this, false);

	mainGameUserEvents = nullptr;
	mainGameUserEvents = arrowGameUserEvents;
	otherGameEvents.push_back(WASDGameUserEvents);

	gameWindow.setFramerateLimit(FRAME_LIMIT);
	gameWindow.clear();
	
	initOwnObjects(this, objectManager);	// tworzenie obiektow poczatkowych
	objectManager->applyBindAllGameEvent();
	gameToolbar = new Toolbar(this);
	
	// komponenty sieciowe
	clientController = nullptr;
	//draw();
	//update();

	//auto a = new ObjectManager(0, this);
	//initOwnObjects(this, a, Space(100,100,1000,1000));	// tworzenie obiektow poczatkowych
	//swap(a, objectManager);
	//delete a;
	//draw();
	//update();
	//networkConnect("192.168.0.12");
	//networkConnect("127.0.0.1");
	networkConnect("192.168.1.101");
}

EngineClient::~EngineClient()
{
	delete clientController;

	// usuwanie wszystkich GameUserEvents
	objectManager->unbindAllGameEvent();
	delete mainGameUserEvents;
	delete gameToolbar;

	auto v = &otherGameEvents;
	for_each(v->begin(), v->end(), [](GameEvents* gameUserEvent)
	{
		delete gameUserEvent;
	});
}

void EngineClient::initOwnObjects(EngineBase* engine, ObjectManager* objectManager, const Space& ownSpace)
{
	// polozenie wzgledne
	static const size_t N = 3;
	static Point points[N] = { Point(50, 50), Point(150, 100), Point(200, 100) };
	Rect<float> ownRect = ownSpace.toFloatRect();

	// zmiana na polozenie bezwzgledne
	// + sprawdzenie czy nie przekroczono wlasnego miejsca
	for (size_t i = 0; i < N; ++i)
	{
		points[i].x += ownSpace.x1;
		points[i].y += ownSpace.y1;
		if (ownRect.contains(points[i].x, points[i].y) == false)
			throw "pkt obiektow nie mieszcza sie w zadanej przestrzeni";
	}

	static vector<GameObject*> objectToAdd;
	objectToAdd.clear();
	objectToAdd.push_back(new Tank1(0, engine, points[1], engine->getMainGameUserEvents(), Color(BORDO)));
	//objectToAdd.push_back(new Tank1(0, engine, points[2], (*engine->getOtherGameEvents())[0], Color::Green));


	// z powodu statycznej metody, nie zawsze jest niezbedna sekcja krytyczna
	// np. dla nie wykorzystywanych jeszcze objectMenagerow
	//if (doInCriticalSection){
	auto section = objectManager->getCriticalSection();
	EnterCriticalSection(section);
	//}

	for (auto it = objectToAdd.begin(); it != objectToAdd.end(); ++it)
		objectManager->addObject(*it);

	//if (doInCriticalSection) 
	LeaveCriticalSection(section);
}

GameCamera* EngineClient::getGameCamera()
{
	auto section = getCriticalSection();
	EnterCriticalSection(section);
	auto tmp = &gameCamera;
	LeaveCriticalSection(section);
	return tmp;
}

void EngineClient::draw()
{
	gameWindow.clear();

	auto section = getCriticalSection();
	EnterCriticalSection(section);

	gameWindow.draw(level->getBackroundSprite());
	//level->draw();
	objectManager->draw();
	gameToolbar->draw();
	gameMenu.draw();

	LeaveCriticalSection(section);

	gameWindow.display();
}

void EngineClient::update()
{
	auto section = getCriticalSection();
	EnterCriticalSection(section);

	if (clientController != nullptr)
		clientController->update();

	auto v = getOtherGameEvents();
	for (auto it = v->begin(); it != v->end(); ++it)
		(*it)->update();

	getMenu()->update();
	// na koncu pozostale dane
	EngineBase::update();

	LeaveCriticalSection(section);
}

bool EngineClient::isOpen()
{
	auto section = getCriticalSection();
	EnterCriticalSection(section);
	auto tmp = gameWindow.isOpen();
	LeaveCriticalSection(section);
	return tmp;
}

RenderWindow* EngineClient::getGameWindow()
{
	auto section = getCriticalSection();
	EnterCriticalSection(section);
	auto tmp = &gameWindow;
	LeaveCriticalSection(section);
	return tmp;
}

GameEvents* EngineClient::getMainGameUserEvents()
{
	auto section = getCriticalSection();
	EnterCriticalSection(section);
	auto tmp = mainGameUserEvents;
	LeaveCriticalSection(section);
	return tmp;
}

vector<GameEvents*>* EngineClient::getOtherGameEvents()
{
	auto section = getCriticalSection();
	EnterCriticalSection(section);
	auto tmp = &otherGameEvents;
	LeaveCriticalSection(section);
	return tmp;
}

void EngineClient::networkConnect(const string& ip_val)
{
	auto section = getCriticalSection();
	EnterCriticalSection(section);

	Object* tmp = clientController;
	addToDeleteInMainThread(&tmp);
	clientController = new ClientController(this, ip_val, ServerController::SERVER_PORT);

	LeaveCriticalSection(section);
}

void EngineClient::networkConnectionEstablish(Json::Value & json)
{
	cout << json << endl;
	size_t id = json[CLIENT_ID].asLargestUInt();
	Space space(json[CREATION_SPACE]);
	auto tmpObjectManager = new ObjectManager(id, this);
	initOwnObjects(this, tmpObjectManager, space);

	auto section = getCriticalSection();
	EnterCriticalSection(section);
	
	this->setObjectManager(tmpObjectManager);
	//tmpObjectManager->applyBindAllGameEvent();
	getObjectManager()->gameEventsOff();		// sterowanie realizowane tylko przez odpowiedzi z serwera
	this->connected = true;

	LeaveCriticalSection(section);

	clientController->start();	// rozpoczecie dzialan odbierania
}

bool EngineClient::isConnected()
{
	return connected;
}

bool EngineClient::isGameRunning()
{
	return gameRunning;
}

void EngineClient::setGameRunning(bool value)
{
	gameRunning = value;
}

void EngineClient::networkDisconnect()
{
	// uwaga mozliwosc zakleszczenie przy nieodpowiednim uzyciu
	auto tmpObjectManager = new ObjectManager(1, this);
	initOwnObjects(this, tmpObjectManager);

	auto section = getCriticalSection();
	EnterCriticalSection(section);
	setObjectManager(tmpObjectManager);
	this->connected = false;
	LeaveCriticalSection(section);
}

Menu* EngineClient::getMenu()
{
	return &gameMenu;
}


