#include "ServerController.hpp"
#include "EngineBase.hpp"
#include "GameUserEvents.hpp"
#include "JsonIndexes.hpp"
#include "DynamicObject.hpp"

#include <iostream>
#include "Tank.hpp"

//const u_short ServerController::SERVER_PORT = 27016;
const Space ServerController::spaces[MAX_CLIENTS] = 
{ 
	{ 0.0f,	0.0f, EngineBase::SCREEN_WIDTH / 2.0f, EngineBase::SCREEN_HEIGHT / 2.0f },
	{ 0.0f, EngineBase::SCREEN_HEIGHT / 2.0f, EngineBase::SCREEN_WIDTH / 2.0f, EngineBase::SCREEN_HEIGHT },
	{ EngineBase::SCREEN_HEIGHT / 2.0f, 0.0f, EngineBase::SCREEN_WIDTH, EngineBase::SCREEN_HEIGHT / 2.0f },
	{ EngineBase::SCREEN_WIDTH / 2.0f, EngineBase::SCREEN_HEIGHT / 2.0f, EngineBase::SCREEN_WIDTH, EngineBase::SCREEN_HEIGHT }
};

ServerController::ServerController(EngineBase* engine, u_short serverPort)
	: 
	NetworkController(engine),
	toRecv(TO_RECV_BUFFOR_SIZE),
	toSend(TO_SEND_BUFFOR_SIZE)
{
	nextId = 2;				// wartosc poczatkowa ID poszeczegolnych klientow
							// wartosc 1 zarezerwowana dla serwera

	const char* addr = "127.0.0.1"; // dla servera adres jest nie wazny
	DWORD thredId;
	serverConnectCfg = { addr, serverPort, engine, nullptr, this };
	
	CreateThread(nullptr, 1 << 24, startServer, static_cast<void*>(&serverConnectCfg), 0, &thredId);
}

ServerController::~ServerController()
{
	engine->getObjectManager()->unbindAllGameEvent();
	for (auto it = remoteGameUserEvents.begin(); it != remoteGameUserEvents.end(); ++it)
		delete (*it).second;
}

ConcurrentQueue<Json::Value*>& ServerController::getToSendQueue()
{
	return toSend;
}

ConcurrentQueue<Json::Value*>& ServerController::getToReceiveQueue()
{
	return toRecv;
}


void ServerController::update()
{
	auto section = engine->getCriticalSection();
	EnterCriticalSection(section);

	auto& toSend = getToSendQueue();
	auto& toRecv = getToReceiveQueue();

	/////////////////// SERIALIZACJA STANU GRY ////////////////////////////
	Json::Value* jsonToSend = new Json::Value;
	engine->serialize(*jsonToSend);
	toSend.push(&jsonToSend);

	/////////////////// ODBIOR ////////////////////////////
	// TODO warunek nie serializacji jesli nie nastapily zmiany od ostatniej 
	Json::Value* jsonToRecv = nullptr;
	if (toRecv.tryPop(&jsonToRecv)){
		auto& json = *jsonToRecv;
		size_t id = json[ID_BINDED_OBJECT].asLargestUInt();

		// dodanie nowego elementu jesli nie istnieje
		if (remoteGameUserEvents.find(id) == remoteGameUserEvents.end())
		{
			remoteGameUserEvents.insert(std::pair<size_t, GameUserEvents*>(id, new GameUserEvents(nullptr)));
			auto bindedObject = engine->getObjectManager()->getObject(id);
			remoteGameUserEvents[id]->setBindedObject(bindedObject);
			
			auto tank = reinterpret_cast<Tank*>(bindedObject);
			tank->bindGameEvents(remoteGameUserEvents[id]);
		}	

		// wlasciwa metoda zmiany ruchu
		remoteGameUserEvents[id]->update(json);
		//engine->getObjectManager()->mergeMovement(*remoteGameUserEvents[id]);


	}
	delete jsonToRecv;

	LeaveCriticalSection(section);
}

DWORD ServerController::serviceSendingSocket(void* serviceCfgVoid)
{
	ServiceCfg* serviceCfg = static_cast<ServiceCfg*>(serviceCfgVoid);
	SOCKET& socket = serviceCfg->socket;
	ServerController* serverController = dynamic_cast<ServerController*>(serviceCfg->networkController);

	Json::Value jsonToSend;
	Json::Value* jsonToSendPointer = nullptr;
	auto engineSection = serviceCfg->engine->getCriticalSection();
	cout << "serviceSendingSocket start" << endl;

	// ############## WYSLANIE INFO INICJALIZACYJNYCH #######################
	auto nextId = serverController->nextId.fetch_add(1); // dodanie i zwrocenie zawartosci
	spaces[nextId % MAX_CLIENTS].serialize(jsonToSend[CREATION_SPACE]);	 // space dla danego gracza
	jsonToSend[CLIENT_ID] = nextId;

	send(socket, jsonToSend);

	// ######################################################################
	try{
		while (true){
			//jsonToSend.clear();
			//
			//serviceCfg->engine->serialize(jsonToSend);
			jsonToSendPointer = serviceCfg->networkController->getToSendQueue().pop();
			//LeaveCriticalSection(engineSection);
			send(socket, *jsonToSendPointer);	
			delete jsonToSendPointer;
			//Sleep(10);
		}
	}catch (ConnectionException e)
	{
		cout << "WYSYL: wylaczono id: " << serviceCfg->engine->getObjectManager()->getId();
	}

	return 0;
}

DWORD ServerController::serviceReceivingSocket(void* serviceCfgVoid)
{
	ServiceCfg* serviceCfg = static_cast<ServiceCfg*>(serviceCfgVoid);
	SOCKET& socket = serviceCfg->socket;
	auto engine = serviceCfg->engine;
	
	Json::Value jsonReceived;
	Json::Value* jsonReceivedPointer = nullptr;
	char buff[BUFF_SIZE];
	string strBuff;
	strBuff.reserve(BUFF_SIZE);
	

	cout << "serviceReceivigSocket start" << endl;
	// odebranienie obiektow stworzonych przez klienta
	recv(socket, jsonReceived, buff, strBuff);
	auto engineSection = serviceCfg->engine->getCriticalSection();
	EnterCriticalSection(engineSection);
	serviceCfg->engine->getObjectManager()->mergeChanges(jsonReceived);		// zatwierdzenie zmian
	LeaveCriticalSection(engineSection);

	// odbieranie gameEventsow
	auto objectManager = serviceCfg->engine->getObjectManager();
	try{
		while (true){
			jsonReceivedPointer = new Json::Value;
			recv(socket, *jsonReceivedPointer, buff, strBuff);
			//EnterCriticalSection(engineSection);
			//cout << *jsonReceivedPointer << endl;
			serviceCfg->networkController->getToReceiveQueue().push(&jsonReceivedPointer);
			//LeaveCriticalSection(engineSection);


			//cout << "dane przed scaleniem: " << jsonReceived << endl;
			//EnterCriticalSection(engineSection);
			//serviceCfg->engine->getObjectManager()->mergeChanges(jsonReceived);		// zatwierdzenie zmian
			//LeaveCriticalSection(engineSection);
			//Sleep(10);
		}
	} catch (ConnectionException e)
	{
		cout << "ODBIOR: wylaczono id: " << serviceCfg->engine->getObjectManager()->getId();
	}

	// usuniecie powiazan, a natepnie wskaznika
	engine->getObjectManager()->unbindAllGameEvent();
	
	return 0;
}
