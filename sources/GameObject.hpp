#pragma once
#ifndef _GAME_OBJECT_HPP_
#define _GAME_OBJECT_HPP_

#include "Space.hpp"
#include "json/json.h"
#include "Object.hpp"
#include <SFML/System/Clock.hpp>

class EngineBase;
class ObjectManager;

using sf::Clock;
using sf::Time;

enum ObjectType
{
	TANK1 = 0,
	TANK2,	
	WALL,
	BULLET,
	EMPTY_OBJECT,
};

enum CanMove{NO_CANT, YES_CAN};

class GameObject abstract : Object
{
public:
	GameObject(size_t id, EngineBase* engine, Space& space);
	GameObject(EngineBase* engine, Json::Value& objectSrc);
	virtual ~GameObject() {}

	virtual void serialize(Json::Value& objectDest);
	virtual void merge(GameObject* gameObject);	
	// bezwarunkowa zmiana Space, bez kolizji
	// przykladowo wszystkie obiekty dynamiczne beda mialy parametr == false
	void merge(GameObject* gameObject, bool isUnconditionalChangeSpace);

	virtual void draw() = 0;
	virtual void update() = 0;					// oparte na czasie w silniku
	virtual void update(Time& deltaTime) = 0;	// oparte na podanym czasie

	virtual int getZIndex() = 0;
	virtual ObjectType getObjectType() = 0;
	virtual int getLives() = 0;
	// jesli zwraca TRUE jesli jest mozliwosc ruchu po zderzeniu
	virtual CanMove collision(GameObject* withObject);
	// metoda wywolywana gdy obiekt osiagnie kraniec mapy lub go przekroczy
	// wywolywana podczas przeszukiwanie kolizji
	virtual void outOfMapEvent() {}

	bool isTransparent();
	size_t getId() const;
	EngineBase* getEngine();
	virtual Space& getSpace() final;
	virtual Time getLifeStartTime() final;
	virtual Time getLifeTime() final;
	
	// dla GameObjectSet
	size_t hash() const;
	bool equal_to(const GameObject& p) const;
	
	// odlaczanie zbindowanych obiektow
	virtual void unbindGameEvents() = 0;
	virtual void applyBindGameEvents() {}
protected:
	void setTransparet();
	
	// zmiana id moze prowadzic do powaznych problemow funkcjonowania sieci
	// nie zmieniac, jesli brak potrzeby
	// utworzone na cele klasy EmptyObject 
	void setId(size_t id);

	EngineBase* engine;
	Space space;
	bool transparentFlag;
private:
	size_t id;	// tylko gettery, zmiana moze zaburzyc funkcjonowanie w sieci
	Time lifeStartTime;
	friend class ObjectManager; // klasa moze podmieniac id, w trakcie ich tworzenia - addObject
};

struct Comparator {
	bool operator() (GameObject* i, GameObject* j) { return (i->getZIndex()<j->getZIndex()); }
};

#endif