#include "Space.hpp"
#include "Point.hpp"
#include "json/json.h"
#include "JsonIndexes.hpp"

#include <cmath>
#include <SFML/Graphics/Transform.hpp>


using sf::Transform;

Space::Space(float x1, float y1, float x2, float y2)
{
	this->x1 = x1;
	this->y1 = y1;
	this->x2 = x2;
	this->y2 = y2;
	this->rotationAngle = 0;
}

Space::Space(int x1, int y1, int x2, int y2)
{
	this->x1 = static_cast<float>(x1);
	this->y1 = static_cast<float>(y1);
	this->x2 = static_cast<float>(x2);
	this->y2 = static_cast<float>(y2);
	this->rotationAngle = 0;
}

Space::Space(int xMiddle, int yMiddle, int width, int height, bool /*nothing*/)
{
	x1 = static_cast<float>(xMiddle) - width / 2.0f;
	y1 = static_cast<float>(yMiddle) - height / 2.0f;
	x2 = x1 + width;
	y2 = y2 + height;
	this->rotationAngle = 0;
}

Space::Space(Json::Value& objectSrc)
{
	x1 = objectSrc[X1].asFloat();
	x2 = objectSrc[X2].asFloat();
	y1 = objectSrc[Y1].asFloat();
	y2 = objectSrc[Y2].asFloat();
	rotationAngle = objectSrc[ROTATION_ANGLE].asFloat();
}

void Space::serialize(Json::Value& objectDest) const
{
	objectDest[X1] = x1;
	objectDest[X2] = x2;
	objectDest[Y1] = y1;
	objectDest[Y2] = y2;
	objectDest[ROTATION_ANGLE] = rotationAngle;
}

Vector2f Space::scale(float destWidth, float destHeight)
{
	float width = static_cast<float>(getWidth());
	float height = static_cast<float>(getHeight());
	float xScale = destWidth / width;
	float yScale = destHeight / height;

	x2 = x1 + destWidth;
	y2 = y1 + destHeight;
	return { xScale, yScale };
}

float Space::getWidth() const
{
	return abs(x2 - x1);
}

float Space::getHeight() const
{
	return abs(y2 - y1);
}

void Space::move(float x, float y)
{
	x1 += x;
	x2 += x;
	y1 += y;
	y2 += y;
}

void Space::setPosition(float x1, float y1, float x2, float y2)
{
	this->x1 = x1;
	this->x2 = x2;
	this->y1 = y1;
	this->y2 = y2;
}

void Space::setPosition(Space& space)
{
	x1 = space.x1;
	x2 = space.x2;
	y1 = space.y1;
	y2 = space.y2;
	rotationAngle = space.getRotation();
}

void Space::setPosition(Point& centerPoint)
{
	move(-getXMid(), -getYMid());		// srodek do 0,0
	move(centerPoint.x, centerPoint.y);	// srodek do point.x, point.y
}

float Space::getRotation()
{
	return rotationAngle;
}

void Space::setRotation(float angle)
{
	Transform transform;

	float xMid = getXMid();
	float yMid = getYMid();

	transform.rotate(angle - rotationAngle, xMid, yMid);

	FloatRect result = transform.transformRect(toFloatRect());
	fromRect(result);

	if (x1 > x2) std::swap(x1, x2);
	if (y1 > y2) std::swap(y1, y2);

	if (angle != 0 && angle != 180 && angle != 90 && angle != 270)
		throw "to Implement";

	rotationAngle = angle;
}

IntRect Space::toIntRect() const
{
	return IntRect(
		static_cast<int>(x1),
		static_cast<int>(y1),
		static_cast<int>(getWidth()),
		static_cast<int>(getHeight())
		);
}

FloatRect Space::toFloatRect() const
{
	return FloatRect(
		static_cast<float>(x1),
		static_cast<float>(y1),
		static_cast<float>(getWidth()),
		static_cast<float>(getHeight())
		);
}

float Space::getYMid()
{
	return y1 + (y2-y1)/ 2;
}

float Space::getXMid()
{
	return x1 + (x2-x1)/ 2;
}
