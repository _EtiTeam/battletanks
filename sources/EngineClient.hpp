#pragma once
#ifndef _ENGINE_CLIENT_HPP_
#define _ENGINE_CLIENT_HPP_

#define FRAME_LIMIT 30

#include "EngineBase.hpp"
#include "Menu.hpp"

#include <SFML/Graphics.hpp>

class ClientController;
class Toolbar;

using namespace sf;

class EngineClient 
	: public EngineBase
{
public:
	EngineClient();
	virtual ~EngineClient();
	
	// tworzenie obiektow poczatkowych
	static void initOwnObjects(EngineBase* engine, ObjectManager* objectManager, const Space& ownSpace = {0.0f, 0.0f, SCREEN_WIDTH, SCREEN_HEIGHT});

	void draw() override;
	void update() override;
	bool isOpen() override;
	
	Menu* getMenu();
	GameCamera* getGameCamera() override;
	RenderWindow* getGameWindow() override;
	GameEvents* getMainGameUserEvents() override;
	vector<GameEvents*>* getOtherGameEvents() override;

	// uwaga mozliwosc zakleszczenia przy nieodpowiednim uzyciu
	void networkDisconnect();
	void networkConnect(const string& ip_val);

	// metoda wywolywana poprzez ClientNetwork po polaczeniu
	// json zawiera inicjalizujące info od serwera
	virtual void networkConnectionEstablish(Json::Value& json);		

	bool isConnected();
	//void setConnected();
	bool isGameRunning();
	void setGameRunning(bool value);
private:
	RenderWindow gameWindow;

	Menu gameMenu;
	Toolbar* gameToolbar;
	GameCamera gameCamera;
	
	GameEvents* mainGameUserEvents;
	vector<GameEvents*> otherGameEvents;

	ClientController* clientController;
	bool connected;
	bool gameRunning;
	

};

#endif