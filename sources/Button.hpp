#pragma once
#ifndef _BUTTON_HPP_
#define _BUTTON_HPP_
#include <SFML/Graphics.hpp>
class EngineClient;

using namespace sf;

class Button
{
	
public:
	Button(EngineClient* engine, int type);

	void draw();

	void setActive(bool value); 
	bool isActive();
	void enter();
	void escape();
private:
	bool active;
	
	EngineClient* engine;
	RectangleShape playBtn;
	Texture playBtnTexture;
	String playBtnPath;
	String playBtnPath_active;
	Vector2u position;
	int type;
	Texture playBtnTexture_active;
};
#endif
