#pragma once
#ifndef _CONCURRENT_QUEUE_HPP_
#define _CONCURRENT_QUEUE_HPP_

#include <thread>
#include <mutex>
#include <condition_variable>
#include <list>

using namespace std;

class NotImplementedException : public std::logic_error
{
public:
	NotImplementedException() :logic_error(what()){}
	char const * what() const override { return "Function not yet implemented."; }
};

template <typename T>
class ConcurrentQueue
{
public:
	ConcurrentQueue()
	{
		throw NotImplementedException();
	}
};


/*	sposob uzycia 
	ConcurrentQueue<int*> queue(2);
	int* a = new int;
	int* b = new int;
	int* c = new int;
	*a = 4; *b = 6; *c = 10;
	queue.push(&a).push(&b).push(&c);
	b = queue.pop();
	delete b;
*/

template <typename T>
class ConcurrentQueue<T*>
{
public:
	ConcurrentQueue()
	{
		maxSize_ = list_.max_size();
	}

	ConcurrentQueue(size_t maxSize)
	{
		maxSize_ = maxSize;
	}

	~ConcurrentQueue()
	{
		clear();
		unique_lock<mutex> mlock(mutex_);
		cond_.notify_all();
		condPush_.notify_all();
	}

	bool isFull()
	{
		unique_lock<mutex> mlock(mutex_);
		return list_.size() >= maxSize_;
	}

	size_t size()
	{
		unique_lock<mutex> mlock(mutex_);
		return list_.size();
	}

	void clear()
	{
		unique_lock<mutex> mlock(mutex_);
		while (!list_.empty())
		{
			delete list_.front();
			list_.pop_front();
		}
	}

	// zwracanie samego siebie do pipeliningu
	// metoda bezpieczna, ustawia wskaznik na nullptr
	ConcurrentQueue<T*>& push(T** item)
	{
		unique_lock<mutex> mlock(mutex_);
		while (isFull_notBlocking())
		{
			delete list_.front();
			list_.pop_front();
		}
		
		// dodanie elementu wraz z notify
		list_.push_back(*item);
		mlock.unlock();
		cond_.notify_one();

		*item = nullptr;
		return *this;
	}

	void blockingPush(T** item)
	{
		unique_lock<mutex> mlock(mutex_);
		while (isFull_notBlocking())
			condPush_.wait(mlock);

		// dodanie elementu wraz z notify
		list_.push_back(*item);
		mlock.unlock();
		cond_.notify_one();

		*item = nullptr;
	}

	// dodaje tylko jesli jest aktalnie miejsce
	bool tryPush(T** item)
	{
		unique_lock<mutex> mlock(mutex_);
		if (list_.size() >= maxSize_)
			return false;

		// dodanie elementu wraz z notify
		list_.push_back(*item);
		mlock.unlock();
		cond_.notify_one();

		*item = nullptr;
		return true;
	}


	bool tryPop(T** result)
	{
		unique_lock<mutex> mlock(mutex_);
		if (list_.empty()){
			*result = nullptr;
			return false;
		}

		*result = list_.front();
		list_.pop_front();
		mlock.unlock();
		condPush_.notify_one();

		return true;
	}
	T* pop()
	{
		unique_lock<mutex> mlock(mutex_);
		while (list_.empty())
		{
			cond_.wait(mlock);
		}
		auto item = list_.front();

		list_.pop_front();
		mlock.unlock();
		condPush_.notify_one();

		return item;
	}
private:

	bool isFull_notBlocking()
	{
		return list_.size() >= maxSize_;
	}
	
	list<T*> list_;
	size_t maxSize_;

	mutex mutex_;
	condition_variable cond_;
	condition_variable condPush_;
};


#endif
