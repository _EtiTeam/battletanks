#pragma once
#ifndef _DYNAMIC_OBJECT_HPP_
#define _DYNAMIC_OBJECT_HPP_

#include "GameObject.hpp"
#include "GameRect.hpp"
#include "Motion.hpp"

class DynamicObject abstract 
	: public GameObject
{
public:
	DynamicObject(size_t id, EngineBase* engine, Space& space);
	DynamicObject(EngineBase* engine, Json::Value& objectSrc);
	virtual ~DynamicObject();

	void serialize(Json::Value& objectDest) override;

	bool setRotation(float angle);						// wraz z uruchomieniem kolizji
	virtual void setSpace(Space& space);				// bezwarunkowa zmiana pozycji

	virtual GameRect* getGameRect() = 0;
	void merge(GameObject* gameObject) override;

	void update() override;
	void update(Time& deltaTime) override final;
	virtual void update(GameEvents* gameEvents) = 0;

	virtual void gameEventsOn() {}
	virtual void gameEventsOff() {}

	bool isCollision(Space& newSpace);		// bez wywolywania kolizji

	// metody dzialajace dla dowolnych argumentow
	CanMove virtual isMovable() = 0;
	virtual bool move(float x, float y) final;			// wraz z uruchomieniem kolizji
	bool setPosition(Space& newSpace);		
	// wraz z uruchomieniem kolizji
protected:
	Motion motion;

private:

	// funkcje dzialajace tylko dla max przesuniecia == 1
	// potrzeba uzycia - uzyj metod bez znaku '_'
	bool _move(float x, float y);							// wraz z uruchomieniem kolizji
	bool _setPosition(Space& newSpace);	// wraz z uruchomieniem kolizji

	// mozliwosc zmiana pozycji obiektu na ktorym operuje
	// tylko klasa Motion moze zmieniac pozycje
	friend class Motion;	
};

#endif