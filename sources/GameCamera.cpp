#include "EngineClient.hpp"
#include "GameCamera.hpp"
#include <SFML/Graphics/RenderWindow.hpp>

GameCamera::GameCamera(EngineClient* engine, GameObject* followed)
{
	this->engine = engine;
	this->followed = followed;
}

void GameCamera::setFollowed(GameObject* followed)
{
	this->followed = followed;
}

Space GameCamera::getSpace()
{
	if (followed != nullptr)
	{
		return followed->getSpace();
	}

	RenderWindow& gameWindow = *engine->getGameWindow();
	return Space(gameWindow.getSize().x / 2, gameWindow.getSize().y / 2, //v TODO zmienic na srodek mapy
		engine->SCREEN_WIDTH, engine->SCREEN_HEIGHT);
}

